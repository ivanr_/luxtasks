<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main menu</title>
</head>
<body>
<h3>Online shop main menu</h3>

<form name="myForm1" action="${pageContext.request.contextPath}/adminMenu" method="get">
    <input type="submit" style="background: #5bfc45;" value="1. Enter to admin menu with password:">
    <label>
        <input type="number" name="adminPassword">
    </label>
    (now not required)
</form>
<br>

<form name="myForm2" action="${pageContext.request.contextPath}/newClientMenu" onsubmit="return validateClientForm()" method="get">
    <input type="submit" style="background: #5bfc45;" value="2. Enter to client menu with id: ">
    <label>
        <input type="number" name="clientId">
    </label>
</form>
<br>

<button type="button" style="background: #5bfc45;" onclick=getAllProducts()>
    3. View products.
</button>
<br>
<br>
Requests for postman: <br>
<u>1. Clients:</u><br>
1.1 Get all clients: <b>GET localhost:8080/clients</b><br>
1.2 Get client by id: <b>GET localhost:8080/clients?id=1</b><br>
1.3 Create new client: <b>POST localhost:8080/clients?name=postna3&surname=postsurn3&age=44&email=postmaiiii@asd.asd&phone=%2B38(055)88277-15</b><br>
1.4 Update client: <b>PUT localhost:8080/clients?name=postna3&surname=postsurn3&age=44&email=postmaiiii@asd.asd&phone=%2B38(055)88277-14&id=100</b><br>
1.5 Delete client by it's id: <b>DELETE localhost:8080/clients?id=92</b><br>
<u>2. Products:</u><br>
2.1 Get all products: <b>GET localhost:8080/products</b><br>
2.2 Get product by it's id: <b>GET localhost:8080/products?id=36</b><br>
2.3 Create new product: <b>POST localhost:8080/products?name=modified Postman Prod&price=37.337</b><br>
2.4 Update product: <b>PUT localhost:8080/products?name=postmanProduct3&price=30.00111&id=40</b><br>
2.5 Delete product by it's id: <b>DELETE localhost:8080/products?id=36</b><br>
<u>3. Orders:</u><br>
3.1 Get all orders (carts) of all clients: <b>GET localhost:8080/orders</b><br>
3.2 Get order (cart) of client by it's id with total sum:<b>GET localhost:8080/orders?clientId=79</b><br>
3.3 Add product to cart of exact client by clientId and productId: <b>POST localhost:8080/orders?clientId=80&productId=1&number=2</b><br>
3.4 Change number of products of client by clientId and productId: <b>PUT localhost:8080/orders?clientId=80&productId=37&number=3</b><br>
3.5 Delete product from client's cart by clientId and productId: <b>DELETE localhost:8080/orders?clientId=80&productId=1</b><br>

<p id="outputLabel"><h1>Label output.</h1>

<script>
    function validateClientForm() {
        var x = document.forms["myForm2"]["clientId"].value;
        if (x === "") {
            alert("Name must be filled out");
            return false;
        }
    }

</script>

<script>

    function getAllProducts() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "/products", true);
        xhttp.send();
    }

</script>

</body>
</html>