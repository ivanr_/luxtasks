<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin menu</title>
</head>
<body>
<h3>Administrator menu (js-based)</h3>

<p>Operations with clients</p>

<button type="button" onclick=addClient(); style="background: #5bfc45;">1. Push to add client</button>
<br>
<form name="clientIdForm">
    <button type="button" onclick=modifyClient(); style="background: #5bfc45;">2. Modify client.</button>
    <label>Id <input type="number" name="id"> </label>
</form>

<form name="clientForm">
    <label>Name <input type="text" name="name"> </label>
    <label>Surname <input type="text" name="surname"> </label>
    <label>Age <input type="number" name="age"> </label>
    <label>E-mail <input type="text" name="email"> </label>
    <label>Phone <input type="text" name="phone"> </label>

</form>
<br>

<form name="clientIdFormForDel">
    <button type="button" onclick=deleteClientById(); style="background: #5bfc45;">3. Remove client by it's id.</button>
    <label> Id <input type="number" name="id"> </label>
</form>
<br>

<button type="button" onclick=getAllClients(); style="background: #5bfc45;">4. View all clients.</button>
<br>

<p>Operations with products</p>
<button type="button" onclick=addProduct(); style="background: #5bfc45;">5. Add product.</button>
<br>
<form name="productIdForm">
    <button type="button" onclick=modifyProduct(); style="background: #5bfc45;">6. Modify product.</button>
    <label> Id <input type="number" name="id"> </label>
</form>

<form name="productForm">
    <label> Name <input type="text" name="name"> </label>
    <label> Price <input type="text" name="price"> </label>
</form>
<br>

<form name="productIdFormForDel">
    <button type="button" onclick=removeProductById(); style="background: #5bfc45;">7. Remove product by it's id.
    </button>
    <label>Id <input type="number" name="id"> </label>
</form>
<br>

<button type="button" onclick=viewAllProductsInfo(); style="background: #5bfc45;">8. View products.</button>
<br>

<p>Operations with orders</p>
<form name="productAddToOrderForm">
    <button type="button" onclick=addProductToOrderForClientById(); style="background: #5bfc45;">9. Add product to cart
        for client.
    </button>
    <label>ProductId to add<input type="number" name="productId"> </label>
    <label>ClientId <input type="number" name="clientId"> </label>
    <label>Product number <input type="number" name="number" value="1"> </label>
</form>

<form name="productDeleteFromOrderForm">
    <button type="button" onclick=removeProductFromOrderForClientById(); style="background: #5bfc45;">10. Remove product
        from cart for client.
    </button>
    <label>ProductId to remove<input type="number" name="productId"> </label>
    <label>ClientId <input type="number" name="clientId"> </label>
</form>
<br>
<button type="button" onclick=viewOrdersAllClients(); style="background: #5bfc45;">11. View cart of all clients.
</button>
<br>
<p>Back to</p>
<form action="/" method="get">
    <input type="submit" value="12. Return to main menu." style="background: #5bfc45;"/>
</form>

<h1>Label output.</h1>
<p id="outputLabel"></p>

<!--1. ADD CLIENT-->
<script>
    function addClient() {
        var clientName = document.forms["clientForm"]["name"].value;
        var clientSurname = document.forms["clientForm"]["surname"].value;
        var clientAge = document.forms["clientForm"]["age"].value;
        var clientEmail = document.forms["clientForm"]["email"].value;
        var clientPhone = document.forms["clientForm"]["phone"].value.replace("+", "%2B");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", "/clients" +
            "?name=" + clientName +
            "&surname=" + clientSurname +
            "&age=" + clientAge +
            "&email=" + clientEmail +
            "&phone=" + clientPhone,
            true);
        xhttp.send();
    }
</script>

<!--2. MODIFY CLIENT-->
<script>
    function modifyClient() {
        var clientId = document.forms["clientIdForm"]["id"].value;
        var clientName = document.forms["clientForm"]["name"].value;
        var clientSurname = document.forms["clientForm"]["surname"].value;
        var clientAge = document.forms["clientForm"]["age"].value;
        var clientEmail = document.forms["clientForm"]["email"].value;
        var clientPhone = document.forms["clientForm"]["phone"].value.replace("+", "%2B");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open('PUT', "/clients" +
            "?id=" + clientId +
            "&name=" + clientName +
            "&surname=" + clientSurname +
            "&age=" + clientAge +
            "&email=" + clientEmail +
            "&phone=" + clientPhone,
            true);
        xhttp.send();
    }
</script>

<!--3. DELETE CLIENT-->
<script>
    function deleteClientById() {
        var clientId = document.forms["clientIdFormForDel"]["id"].value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("DELETE", "/clients?id=" + clientId, true);
        xhttp.send();
    }
</script>

<!--4. GET ALL CLIENTS-->
<script>
    function getAllClients() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "/clients", true);
        xhttp.send();
    }
</script>

<!--5. ADD PRODUCT-->
<script>
    function addProduct() {
        var productName = document.forms["productForm"]["name"].value;
        var productPrice = document.forms["productForm"]["price"].value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", "/products?name=" + productName + "&price=" + productPrice, true);
        xhttp.send();
    }
</script>

<!--6. MODIFY PRODUCT-->
<script>
    function modifyProduct() {
        var productId = document.forms["productIdForm"]["id"].value;
        var productName = document.forms["productForm"]["name"].value;
        var productPrice = document.forms["productForm"]["price"].value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("PUT", "/products?name=" + productName + "&price=" + productPrice + "&id=" + productId, true);
        xhttp.send();
    }
</script>

<!--7. DELETE PRODUCT-->
<script>
    function removeProductById() {
        var productId = document.forms["productIdFormForDel"]["id"].value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("DELETE", "/products?id=" + productId, true);
        xhttp.send();
    }
</script>

<!--8. VIEW PRODUCTS-->
<script>
    function viewAllProductsInfo() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "/products", true);
        xhttp.send();
    }
</script>

<!--9. ADD PRODUCT TO CLIENT'S ORDER-->
<script>
    function addProductToOrderForClientById() {
        var productId = document.forms["productAddToOrderForm"]["productId"].value;
        var clientId = document.forms["productAddToOrderForm"]["clientId"].value;
        var numberOfProduct = document.forms["productAddToOrderForm"]["number"].value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", "/orders?clientId=" + clientId +
            "&productId=" + productId +
            "&number=" + numberOfProduct, true);
        xhttp.send();
    }
</script>

<!--10. REMOVE PRODUCT FROM CART OF CLIENT-->
<script>
    function removeProductFromOrderForClientById() {
        var productId = document.forms["productDeleteFromOrderForm"]["productId"].value;
        var clientId = document.forms["productDeleteFromOrderForm"]["clientId"].value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("DELETE", "/orders?clientId=" + clientId + "&productId=" + productId, true);
        xhttp.send();
    }
</script>

<!--11. VIEW CARTS OF ALL CLIENTS-->
<script>
    function viewOrdersAllClients() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "/orders", true);
        xhttp.send();
    }
</script>


</body>
</html>