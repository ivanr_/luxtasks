<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.URLConnection" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>New client menu</title>
</head>
<body>
<h3>Client new menu (JSP-based)</h3>
<%
    String clientIdStr = request.getParameter("clientId");
    StringBuilder recvbuff = new StringBuilder();
    if (clientIdStr != null) {
        String recv;
        URL jsonpage = new URL("http://localhost:8080/clients?id=" + clientIdStr);
        URLConnection urlcon = jsonpage.openConnection();
        BufferedReader buffread = new BufferedReader(new InputStreamReader(urlcon.getInputStream()));
        while ((recv = buffread.readLine()) != null)
            recvbuff.append(recv);
        buffread.close();
    } else recvbuff.append("client id is absent");
%>

<p>
    Client's data from server: <b id="clientData" style="color: #a0b8fc"><%=recvbuff.toString()%>
</b></p>
<%--used for holding clientId, got from current path--%>
<label style="display: none">Only id <b id="clientIdLabel"><%=clientIdStr%>
</b>
</label><br>

<button type="button" onclick=modifyClient(); style="background: #5bfc45;">1. Modify client.</button>
<span style="font-size: small">Page will reload</span>
<form name="clientForm">
    <label>Name <input type="text" name="name" value="some name"> </label>
    <label>Surname <input type="text" name="surname"> </label>
    <label>Age <input type="number" name="age"> </label>
    <label>E-mail <input type="text" name="email"> </label>
    <label>Phone <input type="text" name="phone"> </label>
</form>
<form name="clientIdFormForDel">
    <button type="button" onclick=deleteCurrentClient(); style="background: #5bfc45;">2. Remove client by it's id.
    </button>
</form>
<br>

<form action="#" method="get">
    <button type="button" onclick=viewAllProductsInfo(); style="background: #5bfc45;">3. View products.</button>
</form>
<br>

<form name="productAddToOrderForm">
    <button type="button" onclick=addProductToOrder(); style="background: #5bfc45;">4. Add product to cart
        for client.
    </button>
    <label>ProductId to add<input type="number" name="productId"> </label>
    <label>Product number <input type="number" name="number" value="1"> </label>
</form>
<form name="productDeleteFromOrderForm">
    <button type="button" onclick=removeProductFromOrder(); style="background: #5bfc45;">5. Remove product
        from cart.
    </button>
    <label>ProductId to remove<input type="number" name="productId"> </label>
</form>
<br>
<button type="button" onclick=viewClientOrder(); style="background: #5bfc45;">6. View cart of client.</button>
<br>
<form action="/">
    <input type="submit" value="7. Return to main menu." style="background: #5bfc45;"/>
</form>
<p id="outputLabel">Output label</p>
<%--TO OUTPUT REQUESTS TO SERVER--%>
<p id="request" style="display: none">Request</p>
<br>
<h3>TEST</h3>
<label><p id="result">default text</p></label>
<button id="testButton" onclick=testFunc()>BUTTON</button>
<script>
    function testFunc(){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("result").innerHTML = this.responseText;
            }
        };
        // var request = "/clients?id=1";
        var request = "/temp";
        var body = {"id":100,
            "name":"bodyName",
            "surName":"bodySurname",
            "age":1000,
            "email":"email100@com.qwe",
            "phone":"+380001234567"};
        document.getElementById("request").innerHTML = request;
        xhttp.open('POST', request, true);
        // xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        // xhttp.onload = requestComplete;
        xhttp.send(JSON.stringify(body));
    }
</script>

<%--0. Onload FILL CLIENT'S data--%>
<script>
    var clientData = document.getElementById("clientData").innerHTML;
    window.onload = function () {
        var clientName = JSON.parse(clientData).message["Founded client"].name;
        var clientSurname = JSON.parse(clientData).message["Founded client"].surName;
        var clientAge = JSON.parse(clientData).message["Founded client"].age;
        var clientEmail = JSON.parse(clientData).message["Founded client"].email;
        var clientPhone = JSON.parse(clientData).message["Founded client"].phone;
        document.forms["clientForm"]["name"].value = clientName;
        document.forms["clientForm"]["surname"].value = clientSurname;
        document.forms["clientForm"]["age"].value = clientAge;
        document.forms["clientForm"]["email"].value = clientEmail;
        document.forms["clientForm"]["phone"].value = clientPhone;
    }
</script>

<!--1. MODIFY CLIENT-->
<script>
    function modifyClient() {
        var clientId = document.getElementById("clientIdLabel").innerHTML;
        var clientName = document.forms["clientForm"]["name"].value;
        var clientSurname = document.forms["clientForm"]["surname"].value;
        var clientAge = document.forms["clientForm"]["age"].value;
        var clientEmail = document.forms["clientForm"]["email"].value;
        var clientPhone = document.forms["clientForm"]["phone"].value.replace("+", "%2B");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        var request = "/clients" +
            "?id=" + clientId +
            "&name=" + clientName +
            "&surname=" + clientSurname +
            "&age=" + clientAge +
            "&email=" + clientEmail +
            "&phone=" + clientPhone;
        document.getElementById("request").innerHTML = request;
        xhttp.open('PUT', request, true);
        xhttp.send();
        setTimeout(function () {
            window.location.reload();
        }, 2000)
    }

</script>

<!--2. REMOVE CLIENT-->
<script>
    function deleteCurrentClient() {
        var clientId = document.getElementById("clientIdLabel").innerHTML;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("DELETE", "/clients?id=" + clientId, true);
        xhttp.send();
        setTimeout(function () {
            window.location = "/";
        }, 2000)
    }
</script>

<!--3. VIEW PRODUCTS-->
<script>
    function viewAllProductsInfo() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "/products", true);
        xhttp.send();
    }
</script>

<!--4. ADD PRODUCT TO CURRENT CLIENT'S ORDER-->
<script>
    function addProductToOrder() {
        var productId = document.forms["productAddToOrderForm"]["productId"].value;
        var clientId = document.getElementById("clientIdLabel").innerHTML;
        var numberOfProduct = document.forms["productAddToOrderForm"]["number"].value;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", "/orders?clientId=" + clientId +
            "&productId=" + productId +
            "&number=" + numberOfProduct, true);
        xhttp.send();
    }
</script>

<!--5. REMOVE PRODUCT FROM CART OF CLIENT-->
<script>
    function removeProductFromOrder() {
        var productId = document.forms["productDeleteFromOrderForm"]["productId"].value;
        var clientId = document.getElementById("clientIdLabel").innerHTML;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        xhttp.open("DELETE", "/orders?clientId=" + clientId + "&productId=" + productId, true);
        xhttp.send();
    }
</script>

<!--6. VIEW CARTS OF ALL CLIENTS-->
<script>
    function viewClientOrder() {
        var xhttp = new XMLHttpRequest();
        var clientId = document.getElementById("clientIdLabel").innerHTML;
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                document.getElementById("outputLabel").innerHTML = this.responseText;
            }
        };
        var request = "/orders?clientId=" + clientId;
        xhttp.open("GET", request, true);
        xhttp.send();
        document.getElementById("request").innerHTML = request;
    }
</script>


</body>
</html>
