package com.romantsev.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Comparator;
import java.util.Objects;

@Entity
@Table(name = "CLIENT")
public class Client implements Comparator {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surName;

    @Column(name = "AGE")
    private int age;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE")
    private String phone;

    public Client() {
    }

    public Client(String name, String surName, int age, String email, String phone) {
        this.name = name;
        this.surName = surName;
        this.age = age;
        this.email = email;
        this.phone = phone;
    }

    public Client(long id, String name, String surName, int age, String email, String phone) {
        this.id = id;
        this.name = name;
        this.surName = surName;
        this.age = age;
        this.email = email;
        this.phone = phone;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                age == client.age &&
                name.equals(client.name) &&
                surName.equals(client.surName) &&
                email.equals(client.email) &&
                phone.equals(client.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surName, age, email, phone);
    }

    @Override
    public int compare(Object o1, Object o2) {
        Client client1 = ((Client) o1);
        Client client2 = ((Client) o2);
        return Long.compare(client1.id, client2.id);
    }
}
