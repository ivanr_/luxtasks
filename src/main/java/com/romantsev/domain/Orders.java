package com.romantsev.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ORDERS")
public class Orders {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private long id;

    @Column(name = "CLIENTID")
    private long clientId;

    @Column(name = "PRODUCTID")
    private long productId;

    @Column(name = "PRODUCTNUMBER")
    private int productNumber;

    public Orders(long clientId, long productId, int productNumber) {
        this.clientId = clientId;
        this.productId = productId;
        this.productNumber = productNumber;
    }

    public Orders(long id, long clientId, long productId, int productNumber) {
        this.id = id;
        this.clientId = clientId;
        this.productId = productId;
        this.productNumber = productNumber;
    }

    public Orders() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public int getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(int productNumber) {
        this.productNumber = productNumber;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orders orders = (Orders) o;
        return id == orders.id &&
                clientId == orders.clientId &&
                productId == orders.productId &&
                productNumber == orders.productNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, productId, productNumber);
    }
}
