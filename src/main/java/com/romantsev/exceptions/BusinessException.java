package com.romantsev.exceptions;

public class BusinessException extends NumberFormatException {

    public BusinessException(String s) {
        super(s);
    }
}
