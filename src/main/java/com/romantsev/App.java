package com.romantsev;

import com.romantsev.view.MainMenu;
import org.h2.tools.Server;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.stream.Stream;

/**
 * The class contains main method for application to launch.
 * This application is dedicated for creating online shop with next DI, REST, CRUD principals.
 *
 * @author Ivan Romantsev
 */
public class App {
    /**
     * Creates main method for running an application
     * Before starting executes initializing of class implementation for dependency injection.
     *
     * @param args - launch options
     * @throws Exception - exceptions for throwing personnel created businessException
     */
    public static void main(String[] args) throws Exception {
//        START H2 SERVER
        Server server = Server.createTcpServer("-tcpAllowOthers", "-tcpPassword", "admin").start();
        System.out.println("H2 database started at URL: " + server.getURL());

//        OUTPUT ALL REGISTERED BEANS
        AnnotationConfigApplicationContext annotationContext = new AnnotationConfigApplicationContext("com");
        System.out.println("BEANS: ");
        Stream
                .of(annotationContext.getBeanDefinitionNames())
                .forEach(System.out::println);

//        BEANS FROM ANNOTATION CONTEXT
//        H2Connector h2Connector = (H2Connector) annotationContext.getBean("h2Connector");
        MainMenu mainMenu = (MainMenu) annotationContext.getBean("mainMenu");

//        h2Connector.startServer();

//        START MAIN
        mainMenu.showMenu();

        server.stop();
        System.out.println("H2 database stopped at URL: " + server.getURL());
    }
}
