package com.romantsev.servlets;

import com.romantsev.db.h2.H2Connector;
import com.romantsev.domain.Product;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ProductService;
import com.romantsev.services.ValidationService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Map;

public class ProductServlet extends HttpServlet {

    private H2Connector connector;
    private ProductService productService;
    private ValidationService validationService;

    ProductServlet(H2Connector connector, ProductService productService, ValidationService validationService) {
        this.connector = connector;
        this.productService = productService;
        this.validationService = validationService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String method = req.getParameter("method");
        if ("put".equals(method)) {
            doPut(req, resp);
            return;
        }
        if ("delete".equals(method)) {
            doDelete(req, resp);
            return;
        }
        if ("post".equals(method)) {
            doPost(req, resp);
            return;
        }
        System.out.println("doGet() method access point");
        String productIdStr = req.getParameter("id");
        if (productIdStr == null) {
            getAllProducts(resp);
        } else {
            getProductById(resp, productIdStr);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if ("put".equals(req.getParameter("method"))) {
            doPut(req, resp);
            return;
        }
        if ("delete".equals(req.getParameter("method"))) {
            doDelete(req, resp);
            return;
        }
        System.out.println("doPost() method access point");
        PrintWriter writer = resp.getWriter();
        String productName = req.getParameter("name");
        BigDecimal productPrice;
        try {
            productPrice = BigDecimal.valueOf(Double.parseDouble(req.getParameter("price")));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            writer.println("Wrong price FORMAT");
            return;
        }
        try {
            connector.startServer();
        } catch (SQLException e) {
            e.printStackTrace();
            writer.println("Db connecting trouble");
        }
        try {
            validationService.verifyProductCreating(new Product(0L, productName, BigDecimal.ONE));
        } catch (BusinessException e) {
            System.out.println("Verification product failed, reason: " + e.getMessage());
            writer.println("Verification product failed, reason: " + e.getMessage());
            return;
        }
        if (productPrice.compareTo(BigDecimal.ZERO) < 0) {
            System.out.println("Price is incorrect, must be positive");
            writer.println("Price is incorrect, must be positive");
            return;
        }
        if (productService.addProductToList(productName, productPrice)) {
            System.out.println("Product was added");
            writer.println("Product was added");
        } else {
            System.out.println("Product adding failed");
            writer.println("Product adding failed");
        }
        connector.stopServer();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("PUT method accessed");
        PrintWriter writer = resp.getWriter();
        long productId;
        BigDecimal productPrice;
        String productName = req.getParameter("name");
        try {
            productId = Long.parseLong(req.getParameter("id"));
            productPrice = BigDecimal.valueOf(Double.parseDouble(req.getParameter("price")));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            writer.println("Wrong product number(id/price) FORMAT");
            return;
        }
        try {
            connector.startServer();
        } catch (SQLException e) {
            e.printStackTrace();
            writer.println("Db connecting trouble");
        }
        try {
            validationService.verifyProductCreating(new Product(0L, productName, BigDecimal.ONE));
        } catch (BusinessException e) {
            System.out.println("Verification product failed, reason: " + e.getMessage());
            writer.println("Verification product failed, reason: " + e.getMessage());
            return;
        }
        if (productPrice.compareTo(BigDecimal.ZERO) < 0) {
            System.out.println("Price is incorrect, must be positive");
            writer.println("Price is incorrect, must be positive");
            return;
        }
        if (productService.modifyProduct(new Product(productId, productName, productPrice))) {
            System.out.println("Product was modified");
            writer.println("Product was modified");
        } else {
            System.out.println("Product modifying failed");
            writer.println("Product modifying failed");
        }
        connector.stopServer();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("DELETE method accessed");
        PrintWriter printWriter = resp.getWriter();
        int id;
        try {
            connector.startServer();
            id = Integer.parseInt(req.getParameter("id"));
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            printWriter.println("Db connection problem");
            return;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong id FORMAT");
            printWriter.println("Wrong id FORMAT");
            return;
        }
        if (productService.removeProductFromListById(id)) {
            System.out.println("product deleted");
            printWriter.println("product deleted");
        } else {
            System.out.println("product NOT deleted");
            printWriter.println("product NOT deleted");
        }
        connector.stopServer();
    }

    private void getProductById(HttpServletResponse resp, String productIdStr) throws IOException {
        resp.setContentType("text/html, encoding=UTF-8");
        PrintWriter writer = resp.getWriter();
        long productIdLong;
        try {
            productIdLong = Long.parseLong(productIdStr);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            writer.println("<p>Wrong productId format</p>");
            return;
        }
        try {
            connector.startServer();
        } catch (SQLException e) {
            System.out.println("<p>Db connection problem</p>");
            e.printStackTrace();
            return;
        }
        Product returnedProduct = productService.getAllProducts().get(productIdLong);
        writer.println("<p>Product data: </p>");
        writer.println("<p>1. Id: " + returnedProduct.getId() + ". </p>");
        writer.println("<p>2. Name: " + returnedProduct.getName() + ". </p>");
        writer.println("<p>3. Price: " + returnedProduct.getPrice() + ". </p>");
        System.out.println(returnedProduct.toString());
        connector.stopServer();
    }

    private void getAllProducts(HttpServletResponse resp) {
        try {
            connector.startServer();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            return;
        }
        Map<Long, Product> allProducts = productService.getAllProducts();
        resp.setContentType("text/html, encoding=UTF-8");
        PrintWriter writer;
        try {
            writer = resp.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        allProducts.forEach((aLong, product) -> {
                    writer.println("<p>" + product.toString() + "</p>");
                    System.out.println(product.toString());
                }
        );
        connector.stopServer();
    }
}
