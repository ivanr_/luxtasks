package com.romantsev.servlets;

import com.romantsev.db.h2.H2Connector;
import com.romantsev.domain.Product;
import com.romantsev.services.OrderService;
import com.romantsev.services.ProductService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

public class OrderServlet extends HttpServlet {

    private H2Connector connector;
    private OrderService orderService;
    private ProductService productService;

    OrderServlet(H2Connector connector, OrderService orderService, ProductService productService) {
        this.connector = connector;
        this.orderService = orderService;
        this.productService = productService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if ("put".equals(req.getParameter("method"))) {
            doPut(req, resp);
            return;
        }
        if ("post".equals(req.getParameter("method"))) {
            doPost(req, resp);
            return;
        }
        if ("delete".equals(req.getParameter("method"))) {
            doDelete(req, resp);
            return;
        }
        System.out.println("access doGet() for order point");
        String clientIdStr = req.getParameter("id");
        if (clientIdStr == null) {
            getAllOrders(resp);
        } else {
            getOrderByClientId(resp, clientIdStr);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("POST method accessed");
        long clientId;
        PrintWriter writer = resp.getWriter();
        try {
            clientId = Long.parseLong(req.getParameter("clientId"));
        } catch (NumberFormatException e) {
            System.out.println("Wrong client id format");
            writer.println("Wrong client id format");
            return;
        }
        System.out.println("Input product id to add at client's cart:");
        writer.println("Input product id to add at client's cart:");
        long productId;
        try {
            productId = Long.parseLong(req.getParameter("productId"));
        } catch (NumberFormatException e) {
            System.out.println("Wrong product id format");
            writer.println("Wrong product id format");
            return;
        }
        try {
            connector.startServer();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("db connection trouble");
            return;
        }
        String productNumberStr = req.getParameter("number");
        int productNumber;
        if (productNumberStr == null) {
            productNumber = 1;
        } else {
            try {
                productNumber = Integer.parseInt(productNumberStr);
            } catch (NumberFormatException e) {
                System.out.println("Wrong FORMAT of product number, used 1");
                writer.println("Wrong FORMAT of product number, used 1");
                productNumber = 1;
            }
        }
        int productCounter = 0;
        while (productCounter < productNumber) {
            if (orderService.addProductToCartById(clientId, productId)) {
                System.out.println("Product with id " + productId + " was added to cart for client with id " + clientId);
                writer.println("<p>Product with id " + productId + " was added to cart for client with id " + clientId + "</p>");
            } else {
                System.out.println("Operation of product adding failed");
                writer.println("Operation of product adding failed");
            }
            productCounter++;
        }
        connector.stopServer();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("PUT method accessed");
        long clientId;
        long productId;
        int productNumber;
        PrintWriter writer = resp.getWriter();
        try {
            clientId = Long.parseLong(req.getParameter("clientId"));
        } catch (NumberFormatException e) {
            System.out.println("Wrong client id format");
            writer.println("Wrong client id format");
            return;
        }
        System.out.println("Input product id to add at client's cart:");
        writer.println("Input product id to add at client's cart:");
        try {
            productId = Long.parseLong(req.getParameter("productId"));
        } catch (NumberFormatException e) {
            System.out.println("Wrong product id format");
            writer.println("Wrong product id format");
            return;
        }
        try {
            connector.startServer();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("db connection trouble");
            return;
        }
        Product currentProduct = productService.getAllProducts().get(productId);
        boolean currentProductIsPresentInClientOrder = orderService.getProductsFromCart(clientId).containsKey(currentProduct);
        if (!currentProductIsPresentInClientOrder) {
            System.out.println("Product is absent in cart of current client");
            writer.println("Product is absent in cart of current client");
            return;
        }
        String productNumberStr = req.getParameter("number");
        if (productNumberStr == null) {
            productNumber = 1;
        } else {
            try {
                productNumber = Integer.parseInt(productNumberStr);
            } catch (NumberFormatException e) {
                System.out.println("Wrong FORMAT of product number, used 1");
                writer.println("Wrong FORMAT of product number, used 1");
                productNumber = 1;
            }
        }
        int productCounter = 0;
        if (!orderService.removeProductFromCartById(clientId, productId)) {
            System.out.println("Change products number fail");
            writer.println("Change products number fail");
            return;
        }
        while (productCounter < productNumber) {
            if (orderService.addProductToCartById(clientId, productId)) {
                System.out.println("Product with id " + productId + " was added to cart for client with id " + clientId);
                writer.println("<p>Product with id " + productId + " was added to cart for client with id " + clientId + "</p>");
            } else {
                System.out.println("Operation of product adding failed");
                writer.println("Operation of product adding failed");
            }
            productCounter++;
        }
        connector.stopServer();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("PUT method accessed");
        PrintWriter writer = resp.getWriter();
        long clientId, productId;
        try {
            clientId = Long.parseLong(req.getParameter("clientId"));
        } catch (NumberFormatException e) {
            System.out.println("Wrong client id format");
            writer.println("Wrong client id format");
            return;
        }
        System.out.println("Input product id to add at client's cart:");
        writer.println("Input product id to add at client's cart:");
        try {
            productId = Long.parseLong(req.getParameter("productId"));
        } catch (NumberFormatException e) {
            System.out.println("Wrong product id format");
            writer.println("Wrong product id format");
            return;
        }
        try {
            connector.startServer();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("db connection trouble");
            return;
        }
        if (orderService.removeProductFromCartById(clientId, productId)) {
            System.out.println("Product with id " + productId + " was removed from client with id " + clientId);
            writer.println("Product with id " + productId + " was removed from client with id " + clientId);
        } else {
            System.out.println("Product with id " + productId + " was NOT removed from client with id " + clientId);
            writer.println("Product with id " + productId + " was NOT removed from client with id " + clientId);
        }
    }

    private void getOrderByClientId(HttpServletResponse resp, String clientIdStr) throws IOException {
        resp.setContentType("text/html, encoding=UTF-8");
        PrintWriter writer = resp.getWriter();
        long clientId;
        try {
            clientId = Long.parseLong(clientIdStr);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            writer.println("<p>Wrong clientId format<p>");
            return;
        }
        try {
            connector.startServer();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            return;
        }
        Map<Product, Integer> clientProducts = orderService.getOrdersOfAllClients().get(clientId);
        writer.println("Your cart contains:");
        clientProducts
                .forEach((product, number) -> {
                    System.out.println(product.toString() + " with " + number + " values");
                    writer.println("<p>" + product.toString() + " with " + number + " values</p>");
                });
        Set<Product> productsSetFromCart = clientProducts.keySet();
        BigDecimal summaryPrice = BigDecimal.ZERO;
        BigDecimal currentProductPrice;
        for (Product productFromCart : productsSetFromCart) {
            currentProductPrice = productFromCart.getPrice();
            BigDecimal numbersOfIdenticalProducts = new BigDecimal(clientProducts.get(productFromCart));
            BigDecimal summaryPositionPrice = currentProductPrice.multiply(numbersOfIdenticalProducts);
            summaryPrice = summaryPrice.add(summaryPositionPrice);
        }
        System.out.println("Summary total: " + summaryPrice);
        writer.println("Summary total: " + summaryPrice);
        connector.stopServer();
    }

    private void getAllOrders(HttpServletResponse resp) {
        try {
            connector.startServer();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            return;
        }
        Map<Long, Map<Product, Integer>> allOrdersFromDb = orderService.getOrdersOfAllClients();
        resp.setContentType("text/html, encoding=UTF-8");
        PrintWriter writer;
        try {
            writer = resp.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Set<Long> clientsId = allOrdersFromDb.keySet();
        for (long clientId : clientsId) {
            Map<Product, Integer> clientOrders = allOrdersFromDb.get(clientId);
            if (clientOrders == null || clientOrders.size() == 0) {
                System.out.println("Client with id " + clientId + " has NO products in cart");
                writer.println("Client with id " + clientId + " has NO products in cart");
                continue;
            }
            System.out.println("Client with id " + clientId + " has next orders");
            writer.println("<p>Client with id " + clientId + " has next orders</p>");
            clientOrders.forEach((product, number) -> {
                System.out.println(product.toString() + " with " + number + " values");
                writer.println("<p>" + product.toString() + " with " + number + " values</p>");
            });
        }
        connector.stopServer();
    }
}
