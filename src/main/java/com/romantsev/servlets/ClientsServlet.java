package com.romantsev.servlets;

import com.romantsev.db.h2.H2Connector;
import com.romantsev.domain.Client;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ClientService;
import com.romantsev.services.ValidationService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Map;

public class ClientsServlet extends HttpServlet {

    private ClientService clientService;

    private H2Connector connector;
    private ValidationService validationService;

    ClientsServlet(ClientService clientService, ValidationService validationService, H2Connector connector) {
        this.clientService = clientService;
        this.connector = connector;
        this.validationService = validationService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if ("put".equals(req.getParameter("method"))) {
            doPut(req, resp);
            return;
        }
        if ("post".equals(req.getParameter("method"))) {
            doPost(req, resp);
            return;
        }
        if ("delete".equals(req.getParameter("method"))) {
            doDelete(req, resp);
            return;
        }
        System.out.println("access doGet() for clients point");
        String clientIdStr = req.getParameter("id");
        if (clientIdStr == null) {
            getAllClients(resp);
        } else {
            getClientById(resp, clientIdStr);
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("POST method accessed");
        int age, id;
        PrintWriter printWriter = resp.getWriter();
        try {
            connector.startServer();
            age = Integer.parseInt(req.getParameter("age"));
            id = Integer.parseInt(req.getParameter("id"));
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            printWriter.println("Db connection problem");
            return;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong age FORMAT");
            printWriter.println("Wrong age FORMAT");
            return;
        }
        Client clientToAdd = new Client(
                id,
                req.getParameter("name"),
                req.getParameter("surname"),
                age,
                req.getParameter("email"),
                req.getParameter("phone")
        );
        try {
            validationService.verifyClientWhileCreating(clientToAdd);
        } catch (BusinessException e) {
            printWriter.println("<p>Client updating fail, reason:\n" + e.getMessage() + "<p>");
            System.out.println("Client updating fail, reason:\n" + e.getMessage());
            e.printStackTrace();
            return;
        }
        clientToAdd = validationService.filtratePhoneSymbols(clientToAdd);
        printWriter.println("got data: ");
        printWriter.println(clientToAdd.toString());
        printWriter.println("DONE response");
        System.out.println(clientToAdd.toString());
        System.out.println("DONE response");
        System.out.println(req.getContextPath());
        if (clientService.modifyClientById(clientToAdd)) {
            printWriter.println("<p>Client was successfully modified</p>");
            System.out.println("Client was successfully modified");
        } else {
            printWriter.println("<p>Client was not modified</p>");
            System.out.println("Client was not modified");
        }
        connector.stopServer();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if ("put".equals(req.getParameter("method"))) {
            doPost(req, resp);
            return;
        }
        if ("delete".equals(req.getParameter("method"))) {
            doDelete(req, resp);
            return;
        }
        System.out.println("POST method accessed");
        int age;
        PrintWriter printWriter = resp.getWriter();
        try {
            connector.startServer();
            age = Integer.parseInt(req.getParameter("age"));
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            printWriter.println("Db connection problem");
            return;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong age FORMAT");
            printWriter.println("Wrong age FORMAT");
            return;
        }
        Client clientToAdd = new Client(
                req.getParameter("name"),
                req.getParameter("surname"),
                age,
                req.getParameter("email"),
                req.getParameter("phone")
        );
        try {
            validationService.verifyClientWhileCreating(clientToAdd);
        } catch (BusinessException e) {
            printWriter.println("<p>Client creating fail, reason:\n" + e.getMessage() + "</p>");
            System.out.println("Client creating fail, reason:\n" + e.getMessage());
            e.printStackTrace();
            return;
        }
        clientToAdd = validationService.filtratePhoneSymbols(clientToAdd);
        printWriter.println("got data: ");
        printWriter.println(clientToAdd.toString());
        printWriter.println("DONE response");
        System.out.println(clientToAdd.toString());
        System.out.println("DONE response");
        System.out.println(req.getContextPath());
        if (clientService.addClient(clientToAdd)) {
            printWriter.println("<p>Client has successfully created</p>");
            System.out.println("Client has successfully created");
        } else {
            printWriter.println("<p>Client was not created</p>");
            System.out.println("Client was not created");
        }
        connector.stopServer();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.println("DELETE method accessed");
        PrintWriter printWriter = resp.getWriter();
        int id;
        try {
            connector.startServer();
            id = Integer.parseInt(req.getParameter("id"));
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            printWriter.println("Db connection problem");
            return;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong id FORMAT");
            printWriter.println("Wrong id FORMAT");
            return;
        }
        if (clientService.deleteClientById(id)) {
            System.out.println("client deleted");
            printWriter.println("client deleted");
        } else {
            System.out.println("client NOT deleted");
            printWriter.println("client NOT deleted");
        }
        connector.stopServer();
    }

    private void getAllClients(HttpServletResponse resp) {
        try {
            connector.startServer();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            return;
        }
        Map<Long, Client> allClients = clientService.getAllClients();
        resp.setContentType("text/html, encoding=UTF-8");
        PrintWriter writer;
        try {
            writer = resp.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        allClients.forEach((aLong, client) -> {
                    writer.println("<p>" + client.toString() + "</p>");
                    System.out.println(client.toString());
                }
        );
        connector.stopServer();
    }

    private void getClientById(HttpServletResponse resp, String clientId) throws IOException {
        resp.setContentType("text/html, encoding=UTF-8");
        PrintWriter writer = resp.getWriter();
        long clientIdLong;
        try {
            clientIdLong = Long.parseLong(clientId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            writer.println("<p>Wrong clientId format<p>");
            return;
        }
        try {
            connector.startServer();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
            return;
        }
        Client returnedClient = clientService.getClientData(clientIdLong);
        writer.println("<p>Client data:</p>");
        writer.println("<p>1. Id: " + returnedClient.getId() + ". </p>");
        writer.println("<p>2. Name: " + returnedClient.getName() + ". </p>");
        writer.println("<p>3. Surname: " + returnedClient.getSurName() + ". </p>");
        writer.println("<p>4. Age: " + returnedClient.getAge() + ". </p>");
        writer.println("<p>5. E-mail: " + returnedClient.getEmail() + ". </p>");
        writer.println("<p>6. Phone: " + returnedClient.getPhone() + ". </p>");
        connector.stopServer();
    }
}
