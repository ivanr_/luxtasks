package com.romantsev.servlets;

import com.romantsev.dao.ClientDao;
import com.romantsev.dao.OrderDao;
import com.romantsev.dao.ProductDao;
import com.romantsev.dao.impl.h2.ClientDaoH2;
import com.romantsev.dao.impl.h2.OrderDaoH2;
import com.romantsev.dao.impl.h2.ProductDaoH2;
import com.romantsev.db.h2.H2Connector;
import com.romantsev.services.ClientService;
import com.romantsev.services.OrderService;
import com.romantsev.services.ProductService;
import com.romantsev.services.ValidationService;
import com.romantsev.services.impl.ClientServiceImpl;
import com.romantsev.services.impl.OrderServiceImpl;
import com.romantsev.services.impl.ProductServiceImpl;
import com.romantsev.services.impl.ValidationServiceImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

//@WebListener
public class WebApp implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        H2Connector connector = new H2Connector();
        ClientDao clientDao = new ClientDaoH2();
        OrderDao orderDao = new OrderDaoH2();
        ProductDao productDao = new ProductDaoH2(orderDao);
        ClientService clientService = new ClientServiceImpl(clientDao);
        ProductService productService = new ProductServiceImpl(productDao);
        OrderService orderService = new OrderServiceImpl(orderDao);
        ValidationService validationService = new ValidationServiceImpl(productService, clientService);

        ServletContext servletContext = sce.getServletContext();
        servletContext
                .addServlet("Clients", new ClientsServlet(clientService, validationService, connector))
                .addMapping("/clients/*");
        servletContext
                .addServlet("Products", new ProductServlet(connector, productService, validationService))
                .addMapping("/products/*");
        servletContext
                .addServlet("Orders", new OrderServlet(connector, orderService, productService))
                .addMapping("/orders/*");
        servletContext
                .addServlet("Root", new RootServlet())
                .addMapping("");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
