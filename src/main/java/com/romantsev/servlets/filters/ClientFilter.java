package com.romantsev.servlets.filters;

import com.romantsev.dao.impl.h2.ClientDaoH2;
import com.romantsev.dao.impl.h2.OrderDaoH2;
import com.romantsev.dao.impl.h2.ProductDaoH2;
import com.romantsev.domain.Client;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ValidationService;
import com.romantsev.services.impl.ClientServiceImpl;
import com.romantsev.services.impl.ProductServiceImpl;
import com.romantsev.services.impl.ValidationServiceImpl;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

public class ClientFilter implements Filter {

    private ValidationService validationService;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String name = servletRequest.getParameter("name");
        String surname = servletRequest.getParameter("surname");
        String age = servletRequest.getParameter("age");
        String email = servletRequest.getParameter("email");
        String phone = servletRequest.getParameter("phone");
        if (name == null && surname == null && age == null && email == null && phone == null) {
            RequestDispatcher dispatcher = servletRequest.getRequestDispatcher("/clients");
            dispatcher.forward(servletRequest, servletResponse);
        }
        try {
            int ageInt = Integer.parseInt(age);
            validationService
                    .verifyClientWhileCreating(
                            new Client(name, surname, ageInt, email, phone));
        } catch (BusinessException e) {
            PrintWriter writer = servletResponse.getWriter();
            writer.println("<h2> WRONG AGE VALUE!!!<h2>");
            writer.println("<h2> Reason: " + e.getMessage() + "<h2>");
            return;
        } catch (NumberFormatException e) {
            PrintWriter writer = servletResponse.getWriter();
            writer.println("<h2> WRONG AGE FORMAT!!!<h2>");
            writer.println("<h2> Reason: " + e.getMessage() + "<h2>");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig filterConfig) {
        validationService = new ValidationServiceImpl(
                new ProductServiceImpl(
                        new ProductDaoH2(
                                new OrderDaoH2()
                        )
                ),
                new ClientServiceImpl(
                        new ClientDaoH2()
                )
        );
    }
}
