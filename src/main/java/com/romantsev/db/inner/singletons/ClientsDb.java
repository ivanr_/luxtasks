package com.romantsev.db.inner.singletons;

import com.romantsev.domain.Client;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

public class ClientsDb {
    private final HashMap<Long, Client> clientMapDb = new HashMap<>();

    private ClientsDb() {
    }

    private static ClientsDb instance;

    public static ClientsDb getInstance() {
        if (instance == null) {
            instance = new ClientsDb();
            instance.clientMapDb.put(0L, new Client(0, "admin", "root admin", 22, "admin@email.com", "+380981234567"));
        }
        return instance;
    }

    public boolean addClient(Client clientToAdd) {
        Set<Long> idKeys = clientMapDb.keySet();
        long maxIdValue = idKeys
                .stream()
                .max(Comparator.comparing(Long::longValue))
                .orElse(0L);
//        Client clientToAdd = new Client(++maxIdValue, name, surName, 0, "", phone);
        clientToAdd.setId(++maxIdValue);
        clientMapDb.put(clientToAdd.getId(), clientToAdd);
        return true;
    }

    public boolean modifyClient(Client client) {
        clientMapDb.put(client.getId(), client);
        return true;
    }

    public boolean removeClientById(long id) {
        clientMapDb.remove(id);
        return true;
    }

    public HashMap<Long, Client> getAllClients() {
        return clientMapDb;
    }

    public Client getClientById(long id) {
        return clientMapDb.get(id);
    }
}
