package com.romantsev.db.inner.singletons;

import com.romantsev.domain.Product;

import java.util.HashMap;
import java.util.Map;

public class ActualOrdersDb {
    private Map<Product, Integer> exactClientOrdersDb = new HashMap<>();
    private final Map<Long, Map<Product, Integer>> allClientsOrdersDb = new HashMap<>();

    private ActualOrdersDb() {
    }

    private static ActualOrdersDb instance;

    public static ActualOrdersDb getInstance() {
        if (instance == null) {
            instance = new ActualOrdersDb();
            instance.exactClientOrdersDb.put(ProductsDb.getInstance().getProductById(0), 1);
            instance.allClientsOrdersDb.put(0L, instance.exactClientOrdersDb);
        }
        return instance;
    }

    public boolean addProductToCartById(long currentClientId, long productId) {
        Product productToAdd = ProductsDb.getInstance().getProductById(productId);
        if (productToAdd == null) {
            return false;
        }
        exactClientOrdersDb = allClientsOrdersDb.get(currentClientId);
        if (exactClientOrdersDb.containsKey(productToAdd)) {
            exactClientOrdersDb.put(productToAdd, exactClientOrdersDb.get(productToAdd) + 1);
        } else {
            exactClientOrdersDb.put(productToAdd, 1);
            allClientsOrdersDb.put(currentClientId, exactClientOrdersDb);
        }

        return true;
    }

    public boolean removeProductById(long currentClientId, long id) {
        Product product = ProductsDb.getInstance().getProductById(id);
        exactClientOrdersDb = allClientsOrdersDb.get(currentClientId);
        exactClientOrdersDb.remove(product);
        allClientsOrdersDb.put(currentClientId, exactClientOrdersDb);
        return true;
    }

    public Map<Product, Integer> getAllProductsFromCart(long currentClientId) {
        return allClientsOrdersDb.get(currentClientId);
    }

    public Map<Long, Map<Product, Integer>> getAllClientsOrdersDb(){
        return allClientsOrdersDb;
    }
}
