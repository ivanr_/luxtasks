package com.romantsev.db.inner.singletons;

import com.romantsev.domain.Product;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

public class ProductsDb {
    private final HashMap<Long, Product> productMapDb = new HashMap<>();

    private static ProductsDb instance;

    private ProductsDb() {
    }

    public static ProductsDb getInstance() {
        if (instance == null) {
            instance = new ProductsDb();
            instance.addProduct(new Product(0, "default product", BigDecimal.valueOf(1.01)));
        }
        return instance;
    }

    public boolean addProduct(Product product) {
        productMapDb.put(product.getId(), product);
        return false;
    }

    public boolean addProductByNamePrice(String name, BigDecimal price) {
        Set<Long> idKeys = productMapDb.keySet();
        long maxIdValue = idKeys
                .stream()
                .max(Comparator.comparing(Long::longValue))
                .orElse(0L);
        Product productToAdd = new Product(maxIdValue + 1, name, price);
        productMapDb.put(maxIdValue + 1, productToAdd);
        return true;
    }

    public boolean removeProductById(long id) {
        productMapDb.remove(id);
        return true;
    }

    public HashMap<Long, Product> getAllProducts() {
        return productMapDb;
    }

    Product getProductById(long id) {
        return productMapDb.get(id);
    }
}
