package com.romantsev.db.h2;

import org.h2.tools.Server;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class H2Connector {
    private Server server;
    public void startServer() throws SQLException {
        server = Server.createTcpServer("-tcpAllowOthers", "-tcpPassword", "admin").start();
        System.out.println("H2 database started at URL: " + server.getURL());
    }

    public void stopServer(){
        server.stop();
        System.out.println("H2 database stopped at URL: " + server.getURL());
    }
}
