package com.romantsev.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.romantsev.domain.Client;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping("/temp")
public class TempController {

    @GetMapping
    @ResponseBody
    public Client getClientById(@RequestBody(required = false) String request) {
        return new Client(
                100,
                "client100",
                "surname100",
                100,
                "email100@com.qwe",
                "+380221234567");
    }

    @PostMapping
    @ResponseBody
    public Client postClient(@RequestBody(required = false) String request) throws IOException {
        System.out.println(request);
        ObjectMapper mapper = new ObjectMapper();
        Client clientFromBody = mapper.readValue(request, Client.class);
        System.out.println("Converted client: " + clientFromBody.toString());
        return new Client(
                111,
                "postMethodReturnedClient",
                "surname100",
                111,
                "email111@com.qwe",
                "+380221234567");
    }
}
