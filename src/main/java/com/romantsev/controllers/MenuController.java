package com.romantsev.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MenuController {

    @GetMapping({"/", "/menu"})
    public String showMainMenu() {
        System.out.println("showMainMenu() method access");
        return "menu";
    }

    @GetMapping("/newClientMenu")
    public String showClientMenu() {
        System.out.println("showClientMenu() method access");
        return "newClientMenu";
    }

    @GetMapping("/adminMenu")
    public String showAdminMenu() {
        System.out.println("showAdminMenu() method access");
        return "adminMenu";
    }
}
