package com.romantsev.controllers;

import com.romantsev.domain.Product;
import com.romantsev.services.OrderService;
import com.romantsev.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/orders")
public class OrderController {

    private OrderService orderService;
    private ProductService productService;

    @Autowired
    public OrderController(OrderService orderService, ProductService productService) {
        this.orderService = orderService;
        this.productService = productService;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doGet(
            @RequestParam(required = false) String clientId
    ) {
        System.out.println("access doGet() for order point");
        if (clientId == null) {
            return getAllOrders();
        } else {
            return getOrderByClientId(clientId);
        }
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doPost(
            @RequestParam(required = false) String clientId,
            @RequestParam(required = false) String productId,
            @RequestParam(required = false) String number
    ) {
        System.out.println("POST method accessed");
        long clientIdLong;
        Map<String, Object> json = new HashMap<>();
        try {
            clientIdLong = Long.parseLong(clientId);
        } catch (NumberFormatException e) {
            System.out.println("Wrong client id format");
            json.put("success", false);
            json.put("message", "Wrong clientId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        System.out.println("Input product id to add at client's cart:");
        long productIdLong;
        try {
            productIdLong = Long.parseLong(productId);
        } catch (NumberFormatException e) {
            System.out.println("Wrong product id format");
            json.put("success", false);
            json.put("message", "Wrong productId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        int productNumber;
        if (number == null) {
            productNumber = 1;
        } else {
            try {
                productNumber = Integer.parseInt(number);
            } catch (NumberFormatException e) {
                System.out.println("Wrong FORMAT of product number, used 1");
                productNumber = 1;
                json.put("caution", "Wrong FORMAT of product number, used 1");
            }
        }
        int productCounter = 0;
        while (productCounter < productNumber) {
            if (orderService.addProductToCartById(clientIdLong, productIdLong)) {
                System.out.println("Product with id " + productId + " was added to cart for client with id " + clientId);
            }
            productCounter++;
        }
        if (productCounter == productNumber) {
            json.put("success", true);
            json.put("message", "Product with id " + productId + " was added to cart for client with id " + clientId);
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            json.put("success", true);
            json.put("message", "Product was added " + productCounter + " times");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doPut(
            @RequestParam(required = false) String clientId,
            @RequestParam(required = false) String productId,
            @RequestParam(required = false) String number
    ) {
        System.out.println("PUT method accessed");
        long clientIdLong, productIdLong;
        int productNumber;
        Map<String, Object> json = new HashMap<>();
        try {
            clientIdLong = Long.parseLong(clientId);
            productIdLong = Long.parseLong(productId);
        } catch (NumberFormatException e) {
            System.out.println("Wrong id format");
            json.put("success", false);
            json.put("message", "Wrong id FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        Product currentProduct = productService.getAllProducts().get(productIdLong);
        boolean currentProductIsPresentInClientOrder =
                orderService.getProductsFromCart(clientIdLong).containsKey(currentProduct);
        if (!currentProductIsPresentInClientOrder) {
            System.out.println("Product is absent in cart of current client");
            json.put("success", false);
            json.put("message", "Product is absent in cart of current client");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        if (number == null) {
            productNumber = 1;
        } else {
            try {
                productNumber = Integer.parseInt(number);
            } catch (NumberFormatException e) {
                System.out.println("Wrong FORMAT of product number, used 1");
                json.put("caution", "Wrong FORMAT of product number, used 1");
                productNumber = 1;
            }
        }
        int productCounter = 0;
        if (!orderService.removeProductFromCartById(clientIdLong, productIdLong)) {
            System.out.println("Change products number fail");
            json.put("success", false);
            json.put("message", "Change products number fail");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        while (productCounter < productNumber) {
            if (orderService.addProductToCartById(clientIdLong, productIdLong)) {
                System.out.println("Product with id " + productId + " was added to cart for client with id " + clientId);
            } else {
                productCounter++;
            }
        }
        if (productCounter == productNumber - 1) {
            json.put("success", true);
            json.put("message", "Product with id " + productId + " was added to cart for client with id " + clientId);
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            json.put("success", true);
            json.put("message", "Product was added " + productCounter + " times");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    @DeleteMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doDelete(
            @RequestParam(required = false) String clientId,
            @RequestParam(required = false) String productId
    ) {
        System.out.println("DELETE method accessed");
        long clientIdLong, productIdLong;
        Map<String, Object> json = new HashMap<>();
        try {
            clientIdLong = Long.parseLong(clientId);
        } catch (NumberFormatException e) {
            System.out.println("Wrong client id format");
            json.put("success", false);
            json.put("message", "Wrong ClientId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        System.out.println("Input product id to add at client's cart:");
        try {
            productIdLong = Long.parseLong(productId);
        } catch (NumberFormatException e) {
            System.out.println("Wrong product id format");
            json.put("success", false);
            json.put("message", "Wrong ProductId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        if (orderService.removeProductFromCartById(clientIdLong, productIdLong)) {
            System.out.println("Product with id " + productId + " was removed from client with id " + clientId);
            json.put("success", true);
            json.put("message", "Product with id " + productId + " was removed from client with id " + clientId);
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            System.out.println("Product with id " + productId + " was NOT removed from client with id " + clientId);
            json.put("success", false);
            json.put("message", "Product with id " + productId + " was NOT removed from client with id " + clientId);
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    private ResponseEntity<Map<String, Object>> getAllOrders() {
        Map<Long, Map<Product, Integer>> allOrdersFromDb = orderService.getOrdersOfAllClients();
        Set<Long> clientsId = allOrdersFromDb.keySet();
        Map<String, Object> message = new HashMap<>();
        Map<String, Object> json = new HashMap<>();
        for (long clientId : clientsId) {
            Map<Product, Integer> clientOrders = allOrdersFromDb.get(clientId);
            if (clientOrders == null || clientOrders.size() == 0) {
                System.out.println("Client with id " + clientId + " has NO products in cart");
                continue;
            }
            System.out.println("Client with id " + clientId + " has next orders");
            message.put(String.valueOf(clientId), clientOrders);
        }
        return getMapResponseEntity(message, json);
    }

    private ResponseEntity<Map<String, Object>> getOrderByClientId(String id) {
        long clientId;
        Map<String, Object> message = new HashMap<>();
        Map<String, Object> json = new HashMap<>();
        try {
            clientId = Long.parseLong(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            json.put("message", "Wrong ProductId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        Map<Product, Integer> clientProducts = orderService.getOrdersOfAllClients().get(clientId);
        if (clientProducts == null) {
            json.put("info", "client's cart empty");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        message.put("Client order", clientProducts);
        Set<Product> productsSetFromCart = clientProducts.keySet();
        BigDecimal summaryPrice = BigDecimal.ZERO;
        BigDecimal currentProductPrice;
        for (Product productFromCart : productsSetFromCart) {
            currentProductPrice = productFromCart.getPrice();
            BigDecimal numbersOfIdenticalProducts = new BigDecimal(clientProducts.get(productFromCart));
            BigDecimal summaryPositionPrice = currentProductPrice.multiply(numbersOfIdenticalProducts);
            summaryPrice = summaryPrice.add(summaryPositionPrice);
        }
        System.out.println("Summary total: " + summaryPrice);
        message.put("Summary total", summaryPrice);
        return getMapResponseEntity(message, json);
    }

    private ResponseEntity<Map<String, Object>> getMapResponseEntity(Map<String, Object> message, Map<String, Object> json) {
        json.put("success", true);
        json.put("message", message);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }
}
