package com.romantsev.controllers;

import com.romantsev.domain.Product;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ProductService;
import com.romantsev.services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/products")
public class ProductController {

    private ProductService productService;
    private ValidationService validationService;

    @Autowired
    public ProductController(ProductService productService, ValidationService validationService) {
        this.productService = productService;
        this.validationService = validationService;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doGet(
            @RequestParam(required = false) String id
            ) {
        System.out.println("GET method accessed");
        System.out.println("access to GET method of client, current clientId param: " + id);
        if (id == null) {
            System.out.println("return allProducts");
            return getAllProducts();
        } else {
            System.out.println("return the only product");
            return getProductById(id);
        }
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doPost(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String price
    ) {
        System.out.println("POST method accessed");
        BigDecimal productPrice;
        Map<String, Object> json = new HashMap<>();
        try {
            productPrice = BigDecimal.valueOf(Double.parseDouble(price));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong price FORMAT");
            json.put("success", false);
            json.put("message", "Wrong price FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        Product productToAdd = new Product(name, productPrice);
        try {
            validationService.verifyProductCreating(productToAdd);
        } catch (BusinessException e) {
            System.out.println("Product creating fail, reason: " + e.getMessage());
            e.printStackTrace();
            json.put("success", false);
            json.put("message", "Product creating fail, reason: " + e.getMessage());
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        System.out.println(productToAdd.toString());
        if (productService.addProductToList(productToAdd.getName(), productToAdd.getPrice())) {
            System.out.println("Product has successfully created");
            json.put("success", true);
            json.put("message", "Product has successfully created");
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            System.out.println("Product was NOT created");
            json.put("success", false);
            json.put("message", "Product was NOT created");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doPut(
            @RequestParam(required = false) String id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String price
    ) {
        System.out.println("PUT method accessed");
        long productId;
        BigDecimal productPrice;
        Map<String, Object> json = new HashMap<>();
        try {
            productId = Long.parseLong(id);
            productPrice = BigDecimal.valueOf(Double.parseDouble(price));
        } catch (NumberFormatException e) {
            System.out.println("Wrong id or price FORMAT");
            json.put("success", false);
            json.put("message", "Wrong id or price FORMAT, " + e.getMessage());
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        Product product = new Product(productId, name, productPrice);
        try {
            validationService.verifyProductCreating(product);
        } catch (BusinessException e) {
            System.out.println("Product updating fail, reason: " + e.getMessage());
            json.put("success", false);
            json.put("message", "Product updating fail, reason: " + e.getMessage());
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        System.out.println(product.toString());
        if (productService.modifyProduct(product)) {
            System.out.println("Product was successfully modified");
            json.put("success", true);
            json.put("message", "Product was successfully modified");
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            System.out.println("Product was NOT modified");
            json.put("success", false);
            json.put("message", "Product was NOT modified");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    @DeleteMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doDelete(@RequestParam(required = false) String id) {
        System.out.println("DELETE method accessed");
        Map<String, Object> json = new HashMap<>();
        long productId;
        try {
            productId = Long.parseLong(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong productId FORMAT");
            json.put("success", false);
            json.put("message", "Wrong productId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.NOT_ACCEPTABLE);
        }
        if (productService.removeProductFromListById(productId)) {
            System.out.println("Product deleted");
            json.put("success", true);
            json.put("message", "Product deleted");
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            System.out.println("Product NOT deleted");
            json.put("success", false);
            json.put("message", "Product NOT deleted");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    private ResponseEntity<Map<String, Object>>  getAllProducts() {
        Map<String, Object> message = new HashMap<>();
        productService
                .getAllProducts()
                .forEach((productId, product) -> {
                    message.put(productId.toString(), product);
                    System.out.println(product.toString());
                });
        return getMapResponseEntity(message);
    }

    private ResponseEntity<Map<String, Object>> getProductById(String id) {
        Map<String, Object> message = new HashMap<>();
        long productId;
        try {
            productId = Long.parseLong(id);
        } catch (NumberFormatException e) {
            Map<String, Object> json = new HashMap<>();
            json.put("success", false);
            json.put("message", "Wrong ProductId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        Product findedProduct = productService.getAllProducts().get(productId);
        if (findedProduct == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        message.put("Founded product", findedProduct);
        return getMapResponseEntity(message);
    }

    private ResponseEntity<Map<String, Object>> getMapResponseEntity(Map<String, Object> message) {
        Map<String, Object> json = new HashMap<>();
        json.put("success", true);
        json.put("message", message);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }
}
