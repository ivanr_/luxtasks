package com.romantsev.controllers;

import com.romantsev.domain.Client;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ClientService;
import com.romantsev.services.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/clients")
public class ClientController {
    //    DATABASE MUST BE LAUNCHED
    private final ClientService clientService;

    private final ValidationService validationService;

    @Autowired
    public ClientController(ClientService clientService, ValidationService validationService) {
        this.clientService = clientService;
        this.validationService = validationService;
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> showClientMenu(
            @RequestParam(required = false) String id
    ) {
        System.out.println("GET method accessed");
        System.out.println("access to GET method of client, current clientId param: " + id);
        if (id == null) {
            System.out.println("return allClients");
            return getAllClients();
        } else {
            System.out.println("return the only client");
            return getClientById(id);
        }
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doPost(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String surname,
            @RequestParam(required = false) String age,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false) String email
    ) {
        System.out.println("POST method accessed");
        int ageInt;
        Map<String, Object> json = new HashMap<>();
        try {
            ageInt = Integer.parseInt(age);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong age FORMAT");
            json.put("success", false);
            json.put("message", "Wrong age FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        Client clientToAdd = new Client(name, surname, ageInt, email, phone);
        try {
            validationService.verifyClientWhileCreating(clientToAdd);
        } catch (BusinessException e) {
            System.out.println("Client creating fail, reason:\n" + e.getMessage());
            e.printStackTrace();
            json.put("success", false);
            json.put(
                    "message",
                    "Client creating fail, reason: " + e.getMessage() +
                            " Pattern: +38(0xx)xxxxxxx/+38(0xx)xxx-xx-xx/+380xxxxx-xx-xx");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        clientToAdd = validationService.filtratePhoneSymbols(clientToAdd);
        System.out.println(clientToAdd.toString());
        System.out.println("DONE response");
        if (clientService.addClient(clientToAdd)) {
            System.out.println("Client has successfully created");
            json.put("success", true);
            json.put("message", "Client has successfully created");
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            System.out.println("Client was not created");
            json.put("success", false);
            json.put("message", "Client was NOT created");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doPut(
            @RequestParam(required = false) String id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String surname,
            @RequestParam(required = false) String age,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false) String email
    ) {
        System.out.println("PUT method accessed");
        int clientAge, clientId;
        Map<String, Object> json = new HashMap<>();
        try {
            clientAge = Integer.parseInt(age);
            clientId = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong age or id FORMAT");
            json.put("success", false);
            json.put("message", "Wrong age or clientId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        Client clientToAdd = new Client(clientId, name, surname, clientAge, email, phone);
        try {
            validationService.verifyClientWhileCreating(clientToAdd);
        } catch (BusinessException e) {
            System.out.println("Client updating fail, reason:\n" + e.getMessage());
            e.printStackTrace();
            json.put("success", false);
            json.put("message", "Client updating fail, reason: " + e.getMessage());
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        clientToAdd = validationService.filtratePhoneSymbols(clientToAdd);
        System.out.println(clientToAdd.toString());
        if (clientService.modifyClientById(clientToAdd)) {
            System.out.println("Client was successfully modified");
            json.put("success", true);
            json.put("message", "Client was successfully modified");
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            System.out.println("Client was not modified");
            json.put("success", false);
            json.put("message", "Client was NOT modified");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    @DeleteMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> doDelete(@RequestParam String id) {
        System.out.println("DELETE method accessed");
        Map<String, Object> json = new HashMap<>();
        long clientId;
        try {
            clientId = Long.parseLong(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            System.out.println("Wrong clientId FORMAT");
            json.put("success", false);
            json.put("message", "Wrong ClientId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        if (clientService.deleteClientById(clientId)) {
            System.out.println("Client deleted");
            json.put("success", true);
            json.put("message", "Client deleted");
            return new ResponseEntity<>(json, HttpStatus.OK);
        } else {
            System.out.println("Client NOT deleted");
            json.put("success", false);
            json.put("message", "Client NOT deleted");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
    }

    private ResponseEntity<Map<String, Object>> getAllClients() {
        Map<String, Object> message = new HashMap<>();
        clientService
                .getAllClients()
                .forEach((currentClientId, client) -> {
                    message.put(currentClientId.toString(), client);
                    System.out.println(client.toString());
                });
        return getMapResponseEntity(message);
    }

    private ResponseEntity<Map<String, Object>> getClientById(String clientId) {
        Map<String, Object> message = new HashMap<>();
        long id;
        try {
            id = Long.parseLong(clientId);
        } catch (NumberFormatException e) {
            Map<String, Object> json = new HashMap<>();
            json.put("success", false);
            json.put("message", "Wrong ClientId FORMAT");
            return new ResponseEntity<>(json, HttpStatus.OK);
        }
        Client findedClient = clientService.getClientData(id);
        if (findedClient == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        message.put("Founded client", findedClient);
        return getMapResponseEntity(message);
    }
    private ResponseEntity<Map<String, Object>> getMapResponseEntity(Map<String, Object> message) {
        Map<String, Object> json = new HashMap<>();
        json.put("success", true);
        json.put("message", message);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");
        return new ResponseEntity<>(json, headers, HttpStatus.OK);
    }
}
