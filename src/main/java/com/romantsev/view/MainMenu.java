package com.romantsev.view;

import com.romantsev.view.abstracts.ShowMenu;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;

/**
 * The class contains main menu for application, provides user to choose enter
 * as administrator or client
 */
@Component
public class MainMenu implements ShowMenu {
    /**
     * Input reader for interact user with applications
     */
    private BufferedReader br;
    /**
     * Holds AdminMenu class implementation for transition to administrator menu
     */
    private AdminMenu adminMenu;
    private ClientMenu clientMenu;


    /**
     * Construct class of object with injected dependencies of administrator or client menus.
     * Accept parameters:
     *
     * @param br         - holds input symbols object for reading data from console
     * @param adminMenu  - holds link to pass inside administrator menu
     * @param clientMenu - holds link to pass inside client menu
     */
    public MainMenu(@Qualifier("getBufferedReader") BufferedReader br, AdminMenu adminMenu, ClientMenu clientMenu) {
        this.br = br;
        this.adminMenu = adminMenu;
        this.clientMenu = clientMenu;
    }

    /**
     * Shows main user menu for proceeding as an administrator or client
     * Allows exit program in usual case without errors
     *
     * @throws Exception - to interrupt application while incorrect input is presented
     */
    public void showMenu() throws Exception {
        boolean isRunning = true;
        while (isRunning) {
            showMainMenu();
            switch (br.readLine()) {
                case "1":
                    adminMenu.showMenu();
                    break;
                case "2":
                    clientMenu.showMenu();
                    break;
                case "0":
                    isRunning = exitMenu();
                    break;
                default:
                    showWithConsole("Wrong input");
                    break;
            }
        }
    }

    /**
     * Provides exit function to main menu with
     *
     * @return - boolean value of running application end
     * @throws InterruptedException - uses thread interrupt for console output
     */
    private boolean exitMenu() throws InterruptedException {
        System.out.print("Exiting appication.");
        Thread.sleep(1000);
        System.out.print(".");
        Thread.sleep(1000);
        System.out.print(".");
        Thread.sleep(1000);
        System.out.print(" Done");
        return false;
    }

    /**
     * Show menus options for user to choose next actions
     */
    private void showMainMenu() {
        showWithConsole("Main menu");
        showWithConsole("1. Admin menu");
        showWithConsole("2. Client menu");
        showWithConsole("0. Exit");
    }

    /**
     * Shows inputed value through console
     *
     * @param input - accepted value for console output
     */
    private void showWithConsole(String input) {
        System.out.println(input);
    }
}
