package com.romantsev.view.abstracts;

/**
 * The interface provides possibilities of output data from application to user
 */
public interface ShowMenu {
    /**
     * Shows menu to user
     * @throws Exception - to intercept incorrect execution instead of crashing while output
     */
    void showMenu() throws Exception;
}
