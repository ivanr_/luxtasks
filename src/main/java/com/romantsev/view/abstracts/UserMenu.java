package com.romantsev.view.abstracts;

import com.romantsev.services.ClientService;
import com.romantsev.services.OrderService;
import com.romantsev.services.ProductService;
import com.romantsev.services.ValidationService;

import java.io.BufferedReader;

public abstract class UserMenu implements ShowMenu{

    /**
     * Input reader for interact user with applications
     */
    protected BufferedReader br;

    /**
     * Holds the service for CLIENT managing: creating, modifying, viewing, removing
     */
    protected ClientService clientService;

    /**
     * Holds the service for PRODUCT managing: creating, modifying, viewing, removing
     */
    protected ProductService productService;


    /**
     * Holds the service for ORDERS to manage: adding product to cart instead client by it's ids
     */
    protected OrderService orderService;

    /**
     * Holds the service for VALIDATION of:
     * - client data (phone matching and allowed expression)
     * - product data (product name matching)
     */
    protected ValidationService validationService;
}
