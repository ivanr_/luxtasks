package com.romantsev.view;

import com.romantsev.domain.Client;
import com.romantsev.domain.Product;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ClientService;
import com.romantsev.services.OrderService;
import com.romantsev.services.ProductService;
import com.romantsev.services.ValidationService;
import com.romantsev.view.abstracts.ShowMenu;
import com.romantsev.view.abstracts.UserMenu;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;

/**
 * This class provides client rights to:
 * - view, modify, remove personnel data
 * - view products database
 * - add ans remove products to cart, view cart (order)
 */
@Component
public class ClientMenu extends UserMenu implements ShowMenu {

    /**
     * contains input client id to interact with correct cart and client info
     */
    private long clientCurrentId = -1;

    /**
     * Constructs the object of client's menu to make an orders from products database
     * @param br allows input data from console
     * @param clientService let the user to interact with his own personnel data
     * @param productService provides info about products database and let user to make orders
     * @param orderService is used for placing and removing products from cart
     * @param validationService is used for verification consistence product's and client's data to add in database
     */
    public ClientMenu(@Qualifier("getBufferedReader") BufferedReader  br,
                      ClientService clientService,
                      ProductService productService,
                      OrderService orderService,
                      ValidationService validationService) {
        this.br = br;
        this.clientService = clientService;
        this.productService = productService;
        this.orderService = orderService;
        this.validationService = validationService;
    }

    /**
     * shows client's menu
     * @throws Exception if presents fail during input data from console
     */
    @Override
    public void showMenu() throws Exception {
        boolean isRunning = true;
        showWithConsole("Enter client id ");
        try {
            clientCurrentId = Long.parseLong(br.readLine());
            viewClientInfo();
        } catch (NumberFormatException | NullPointerException e) {
            showWithConsole("wrong input id value");
            if (!createNewClientWhenEnter()) {
                return;
            }
        }
        while (isRunning) {
            showClientMenu();
            switch (br.readLine()) {
                case "1":
                    viewClientInfo();
                    break;
                case "2":
                    modifyClientById();
                    break;
                case "3":
                    removeClientById();
                    showWithConsole("Returning to main menu\n");
                    return;
                case "4":
                    showAllProducts();
                    break;
                case "5":
                    addProductToCartById();
                    break;
                case "6":
                    removeProductFromCartById();
                    break;
                case "7":
                    viewCart();
                    break;
                case "9":
                    showWithConsole("Back to: ");
                    isRunning = false;
                    break;
                case "0":
                    System.exit(0);
                    break;
                default:
                    showWithConsole("wrong input");
            }
        }
    }

    /**
     * Creates new client while enter to client's interface if client is absent
     * @return approve if client was created with success
     * @throws IOException during fail while input data from console
     */
    private boolean createNewClientWhenEnter() throws IOException {
        showWithConsole("To create new client input 1, to return - any key");
        int inputChoice = 0;
        try {
            inputChoice = Integer.parseInt(br.readLine());
        } catch (NumberFormatException | IOException e) {
            showWithConsole("Client creating fail, reason:");
            showWithConsole(e.getMessage());
        }
        if (inputChoice == 1) {
            Client clientToAdd = getClientDataExceptId();
            try {
                validationService.verifyClientWhileCreating(clientToAdd);
            } catch (BusinessException e) {
                showWithConsole("Client creating fail");
                e.printStackTrace();
                return false;
            }
            clientToAdd = validationService.filtratePhoneSymbols(clientToAdd);
            if (clientService.addClient(clientToAdd)) {
                Set<Long> keySet = clientService.getAllClients().keySet();
                clientCurrentId = keySet
                        .stream()
                        .max(Comparator.comparing(Long::longValue))
                        .orElseThrow(ArrayIndexOutOfBoundsException::new);
                showWithConsole("new client was created with id: " + clientCurrentId);
                return true;
            }
        }
        return false;
    }

    /**
     * Provides all client's data input from console except id
     * @return client data without id
     * @throws IOException during fail while input data from console
     */
    private Client getClientDataExceptId() throws IOException {
        Client clientToAdd = new Client();
        showWithConsole("Input name");
        clientToAdd.setName(br.readLine());
        showWithConsole("Input surName");
        clientToAdd.setSurName(br.readLine());
        showWithConsole("Input phone (accepted format: +38(0xx)xxx-xx-xx or +38(0xx)xxxxxxx, \"x - any digit \" )");
        showWithConsole("The number MUST not match with any of clients database");
        clientToAdd.setPhone(br.readLine());
        showWithConsole("Enter age");
        clientToAdd.setAge(Integer.parseInt(br.readLine()));
        showWithConsole("Enter new email");
        clientToAdd.setEmail(br.readLine());
        return clientToAdd;
    }

    /**
     * Output current user's cart through console
     * Uses client id that input when enter
     */
    private void viewCart() {
        Map<Product, Integer> productsFromCart = orderService.getProductsFromCart(clientCurrentId);
        if (productsFromCart == null || productsFromCart.size() == 0) {
            showWithConsole("Your cart is empty");
            return;
        }
        showWithConsole("Your cart contains:");
        productsFromCart
                .forEach((product, number) -> showWithConsole(product.toString() + " with " + number + " values"));
        Set<Product> productsSetFromCart = productsFromCart.keySet();
        BigDecimal summaryPrice = BigDecimal.ZERO;
        BigDecimal currentProductPrice;
        for (Product productFromCart : productsSetFromCart) {
            currentProductPrice = productFromCart.getPrice();
            BigDecimal numbersOfIdenticalProducts = new BigDecimal(productsFromCart.get(productFromCart));
            BigDecimal summaryPositionPrice = currentProductPrice.multiply(numbersOfIdenticalProducts);
            summaryPrice = summaryPrice.add(summaryPositionPrice);
        }
        showWithConsole("Summary total: " + summaryPrice);
    }

    /**
     * Remove product from cart within using input product id from user
     * @throws IOException if console input was failed
     */
    private void removeProductFromCartById() throws IOException {
        showWithConsole("Input id product");
        long productId;
        try {
            productId = Long.parseLong(br.readLine());
        } catch (NumberFormatException e) {
            showWithConsole("Product id doesn't exist");
            return;
        }
        if (orderService.removeProductFromCartById(clientCurrentId, productId)) {
            showWithConsole("Product was removed with success");
        }
    }

    /**
     * Allow user to add some product to it's own cart within input productId
     * @throws IOException if input console data failed
     */
    private void addProductToCartById() throws IOException {
        showWithConsole("Input id product");
        long productId;
        try {
            productId = Long.parseLong(br.readLine());
        } catch (NumberFormatException e) {
            showWithConsole("Product id doesn't exist");
            return;
        }
        if (orderService.addProductToCartById(clientCurrentId, productId)) {
            showWithConsole("Product was successfully added to cart");
        } else showWithConsole("Product adding failed");
    }

    /**
     * Output all products data from product database
     */
    private void showAllProducts() {
        Map<Long, Product> productsFromDb = productService.getAllProducts();
        showWithConsole("List of accessible products");
        productsFromDb.forEach((aLong, product) -> showWithConsole(product.toString()));
        if (productsFromDb.size() == 0) showWithConsole("Db product us empty");
    }

    /**
     * Removes current user by it's id, needs approving id to execute this operation
     * @throws IOException if input console data fails
     */
    private void removeClientById() throws IOException {
        showWithConsole("To remove client input it's id");
        long id = Long.parseLong(br.readLine());
        clientService.deleteClientById(id);
        showWithConsole("Client removed");
    }

    /**
     * Modifies client private information
     * @throws Exception if input console data fails
     */
    private void modifyClientById() throws Exception {
        showWithConsole("To modify client input client id");
        long id = Long.parseLong(br.readLine());
        Client client = getClientDataExceptId();
        client.setId(id);
        try {
            validationService.verifyClientWhileCreating(client);
        } catch (BusinessException e) {
            showWithConsole("Client modifying fail");
            e.printStackTrace();
            return ;
        }
        client = validationService.filtratePhoneSymbols(client);
        if (clientService.modifyClientById(client)) {
            showWithConsole("Client data was modified");
        } else {
            showWithConsole("Modification client error!");
        }
    }

    /**
     * Output client data to console than is called with public method
     */
    private void viewClientInfo() {
        Client client = clientService.getClientData(clientCurrentId);
        try {
            showWithConsole("Reserved client data with id " + client.getId());
            showWithConsole("- name: " + client.getName());
            showWithConsole("- surname: " + client.getSurName());
            showWithConsole("- phone: " + client.getPhone());
            showWithConsole("- age: " + client.getAge());
            showWithConsole("- email: " + client.getEmail());
        } catch (NullPointerException e) {
            showWithConsole("View client info status: FAIL");
            throw new NullPointerException("Client doesn't exist or database is unavailable");
        }
    }

    /**
     * Shows inputed data through console
     * @param input provides message to output
     */
    private void showWithConsole(String input) {
        System.out.println(input);
    }

    /**
     * Shows client menu using console
     */
    private void showClientMenu() {
        showWithConsole("Client menu");
        showWithConsole("1. View client info");
        showWithConsole("2. Modify client data");
        showWithConsole("3. Remove client data");
        showWithConsole("4. Get products info");
        showWithConsole("5. Add product to cart");
        showWithConsole("6. Remove product from cart");
        showWithConsole("7. View cart");
        showWithConsole("9. Return");
        showWithConsole("0. Exit");
    }
}
