package com.romantsev.view;

import com.romantsev.domain.Client;
import com.romantsev.domain.Product;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ClientService;
import com.romantsev.services.OrderService;
import com.romantsev.services.ProductService;
import com.romantsev.services.ValidationService;
import com.romantsev.view.abstracts.ShowMenu;
import com.romantsev.view.abstracts.UserMenu;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

/**
 * This class provides user administrate rights to rule next:
 * - clients
 * - orders
 * - products
 */
@Component
public class AdminMenu extends UserMenu implements ShowMenu {


    /**
     * @param br                - reader for input data from client
     * @param clientService     - client interaction service
     * @param productService    - product interaction service
     * @param validationService - validation service for client and product
     */
    public AdminMenu(@Qualifier("getBufferedReader") BufferedReader br,
                     ClientService clientService,
                     ProductService productService,
                     ValidationService validationService,
                     OrderService orderService) {
        this.br = br;
        this.clientService = clientService;
        this.productService = productService;
        this.validationService = validationService;
        this.orderService = orderService;
    }

    /**
     * Shows administrator menu, input data from client, presents user console interface
     *
     * @throws Exception is used for throwing IOException and BusinessException while executing interface actions
     */
    @Override
    public void showMenu() throws Exception {
        boolean isRunning = true;
        while (isRunning) {
            showAdminMenu();

            switch (br.readLine()) {
                case "1":
                    addClient();
                    break;
                case "2":
                    modifyClient();
                    break;
                case "3":
                    deleteClientById();
                    break;
                case "4":
                    viewAllClients();
                    break;
                case "5":
                    addProduct();
                    break;
                case "6":
                    modifyProductById();
                    break;
                case "7":
                    removeProductById();
                    break;
                case "8":
                    viewAllProductsInfo();
                    break;
                case "9":
                    addProductToOrderForClientById();
                    break;
                case "10":
                    removeProductFromOrderForClientById();
                    break;
                case "11":
                    viewOrdersAllClients();
                    break;
                case "12":
                    showWithConsole("Back to: ");
                    isRunning = false;
                    break;
                case "13":
                    System.exit(0);
                    break;
                default:
                    showWithConsole("wrong input");
            }
        }
    }

    /**
     * Shows orders for all clients when they product list isn't empty
     */
    private void viewOrdersAllClients() {
        Map<Long, Map<Product, Integer>> allOrdersFromDb = orderService.getOrdersOfAllClients();
        Set<Long> clientsId = allOrdersFromDb.keySet();
        for (long client : clientsId) {
            Map<Product, Integer> clientOrders = allOrdersFromDb.get(client);
            if (clientOrders == null || clientOrders.size() == 0) {
                showWithConsole("Client with id " + client + " has NO products in cart");
                continue;
            }
            showWithConsole("Client with id " + client + " has next orders");
            clientOrders.forEach((product, number) -> showWithConsole(product.toString() + " with " + number + " values"));
        }
    }

    /**
     * Removes product from clients cart, accepts client id and product id through console
     *
     * @throws IOException - is used because of interact with user through console failed
     */
    private void removeProductFromOrderForClientById() throws IOException {
        showWithConsole("Input client id :");
        long clientId;
        try {
            clientId = Long.parseLong(br.readLine());
        } catch (NumberFormatException e) {
            showWithConsole("Wrong client id format");
            return;
        }
        showWithConsole("Input product id to remove from client's cart:");
        long productId;
        try {
            productId = Long.parseLong(br.readLine());
        } catch (NumberFormatException e) {
            showWithConsole("Wrong product id format");
            return;
        }
        if (orderService.removeProductFromCartById(clientId, productId)) {
            showWithConsole("Product with id " + productId + " was removed from cart for client with id " + clientId);
        } else showWithConsole("Operation of product removing failed");
    }

    /**
     * Adds product to clients cart, accepts client id and product id through console
     *
     * @throws IOException - is used because of interact with user through console failed
     */
    private void addProductToOrderForClientById() throws IOException {
        showWithConsole("Input client id :");
        long clientId;
        try {
            clientId = Long.parseLong(br.readLine());
        } catch (NumberFormatException e) {
            showWithConsole("Wrong client id format");
            return;
        }
        showWithConsole("Input product id to add at client's cart:");
        long productId;
        try {
            productId = Long.parseLong(br.readLine());
        } catch (NumberFormatException e) {
            showWithConsole("Wrong product id format");
            return;
        }
        if (orderService.addProductToCartById(clientId, productId)) {
            showWithConsole("Product with id " + productId + " was added to cart for client with id " + clientId);
        } else showWithConsole("Operation of product adding failed");
    }

    /**
     * Modify product by it's id from database
     *
     * @throws IOException - is used because of interact with user through console failed
     */
    private void modifyProductById() throws IOException {
        showWithConsole("Input product id:");
        long productId;
        try {
            productId = Long.parseLong(br.readLine());
        } catch (NumberFormatException e) {
            showWithConsole("Wrong product id format");
            return;
        }
        String name = getStringAfterMessage("Input new product name:");
        String priceStr = getStringAfterMessage("Input new product price:");
        try {
            BigDecimal price = new BigDecimal(priceStr);
            if (productService.modifyProduct(new Product(productId, name, price))) {
                showWithConsole("Product was added");
            } else showWithConsole("Product adding failed!");
        } catch (NumberFormatException e) {
            showWithConsole("Wrong price format!");
        }
    }

    /**
     * Delete client by it's id
     *
     * @throws IOException is used for catching/throwing the IOException while input data from client
     */
    private void deleteClientById() throws IOException {
        showWithConsole("Enter client id to remove");
        long clientId;
        try {
            clientId = Long.parseLong(br.readLine());
        } catch (NumberFormatException e) {
            showWithConsole("Wrong client id parameter");
            return;
        }
        if (clientService.deleteClientById(clientId)) {
            showWithConsole("Client deleted");
        } else {
            showWithConsole("Error, client was not deleted");
        }
    }

    /**
     * Let view data of all products
     */
    private void viewAllProductsInfo() {
        Map<Long, Product> productsMapDb = productService.getAllProducts();
        productsMapDb.forEach((aLong, product) -> {
            if (product != null) showWithConsole(product.toString());
        });
        showWithConsole("Products data:");
    }

    /**
     * Let see data of all registered clients
     */
    private void viewAllClients() {
        Map<Long, Client> clientsDb = clientService.getAllClients();
        if (clientsDb != null) {
            clientsDb.forEach((aLong, client) -> System.out.println(client.toString()));
        } else {
            System.out.println("Clients database empty");
        }
    }

    /**
     * Let the user/administrator remove any user by explicit id
     *
     * @throws IOException is used for catching/throwing the IOException while input data from client
     */
    private void removeProductById() throws IOException {
        showWithConsole("Input productId");
        try {
            long productId = Long.parseLong(br.readLine());
            if (productService.removeProductFromListById(productId)) {
                showWithConsole("Product with id " + productId + " is not present anymore in database");
            } else {
                showWithConsole("Product not found");
            }
        } catch (NumberFormatException ex) {
            showWithConsole("Wrong id product");
        }
    }

    /**
     * Adds product to product database with name and price of added product,
     * validate name to provide unique name not existing in database,
     * provides 2 attempts to input name value,
     * verify price name for positive value.
     *
     * @throws IOException is used for catching/throwing the IOException while input data from client
     */
    private void addProduct() throws IOException {
        String name = getStringAfterMessage("Input product name:");
        try {
            validationService.verifyProductCreating(new Product(0L, name, BigDecimal.ONE));
        } catch (BusinessException e) {
            showWithConsole("Verification product failed, reason: " + e.getMessage());
            name = getStringAfterMessage("Input product name again");
        }
        try {
            validationService.verifyProductCreating(new Product(0L, name, BigDecimal.ONE));
        } catch (BusinessException e) {
            showWithConsole("Verification product failed, reason: " + e.getMessage());
            return;
        }
        String priceStr = getStringAfterMessage("Input product price:");
        try {
            BigDecimal price = new BigDecimal(priceStr);
            if (price.compareTo(BigDecimal.ZERO) < 0) {
                showWithConsole("Price is incorrect, must be positive");
                return;
            }
            if (productService.addProductToList(name, price)) {
                showWithConsole("Product was added");
            } else showWithConsole("Product adding failed");
        } catch (
                NumberFormatException e) {
            showWithConsole("Wrong price format!");
        }

    }

    /**
     * @throws Exception is used for
     *                   - BusinessException throwing while validating client
     *                   - IOException throwing while input client data
     */
    private void addClient() throws Exception {
        Client clientToAdd = getClientDataExceptId();

        try {
            validationService.verifyClientWhileCreating(clientToAdd);
        } catch (BusinessException e) {
            showWithConsole("Client creating fail, reason:");
            e.printStackTrace();
            return;
        }

        clientToAdd = validationService.filtratePhoneSymbols(clientToAdd);
        if (clientService.addClient(clientToAdd)) {
            showWithConsole("Client has successfully created");
        } else {
            showWithConsole("Client was not created");
        }

    }

    /**
     * Provides client data except client id
     *
     * @return returns client class data
     * @throws IOException is used for catching/throwing the IOException while input data from client
     */
    private Client getClientDataExceptId() throws IOException {
        Client clientToAdd = new Client();
        clientToAdd.setName(getStringAfterMessage("Input name"));
        clientToAdd.setSurName(getStringAfterMessage("Input surName"));
        clientToAdd.setPhone(getStringAfterMessage(
                "Input phone (accepted format: +38(0xx)xxx-xx-xx or +38(0xx)xxxxxxx, \"x - any digit \" )\n" +
                        "\"The number MUST not match with any of clients database\""));
        showWithConsole("Enter age, no less than 18");
        try {
            clientToAdd.setAge(Integer.parseInt(br.readLine()));
        } catch (NumberFormatException e) {
            e.getMessage();
            clientToAdd.setAge(0);
        } catch (IOException e) {
            e.printStackTrace();
            clientToAdd.setAge(0);
        }
        clientToAdd.setEmail(getStringAfterMessage("Enter new email (up to 60 characters length)"));
        return clientToAdd;
    }

    /**
     * Modifies client data by it's id providing
     *
     * @throws Exception is used for catching/throwing the IOException while input data from client
     */
    private void modifyClient() throws Exception {
        showWithConsole("To modify client is needed to change name, surname and phone");
        showWithConsole("To modify press 1");
        if (br.readLine().equals("1")) {
            showWithConsole("Enter client id");
            long clientId = Long.parseLong(br.readLine());
            Client clientToAdd = getClientDataExceptId();
            clientToAdd.setId(clientId);
            try {
                validationService.verifyClientWhileCreating(clientToAdd);
            } catch (BusinessException e) {
                showWithConsole("Client modifying fail");
                e.printStackTrace();
                return;
            }
            clientToAdd = validationService.filtratePhoneSymbols(clientToAdd);
            boolean result = clientService.modifyClientById(clientToAdd);
            if (result) {
                showWithConsole("Client was modified");
            } else {
                showWithConsole("Client was not modified");
            }
        }
    }

    /**
     * Shows administrator menu through console
     */
    private void showAdminMenu() {
        showWithConsole("Admin menu");
        showWithConsole("1. Add client");
        showWithConsole("2. Modify client");
        showWithConsole("3. Remove client");
        showWithConsole("4. View clients data");
        showWithConsole("5. Add product");
        showWithConsole("6. Modify product");
        showWithConsole("7. Remove product");
        showWithConsole("8. View product database");
        showWithConsole("9. Add product of order instead of client");
        showWithConsole("10. Remove product of order instead of client");
        showWithConsole("11. List of all orders for each client");
        showWithConsole("12. Return to main menu");
        showWithConsole("13. Exit");
    }

    /**
     * Outputs data through console
     *
     * @param input accepts data to output through console
     */
    private void showWithConsole(String input) {
        System.out.println(input);
    }


    /**
     * Provide String data from console after message was shown
     *
     * @param message accept the text to output
     * @return String type data from console
     * @throws IOException if console input fails
     */
    private String getStringAfterMessage(String message) throws IOException {
        showWithConsole(message);
        return br.readLine();
    }
}