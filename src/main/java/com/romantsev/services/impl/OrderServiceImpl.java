package com.romantsev.services.impl;

import com.romantsev.dao.OrderDao;
import com.romantsev.domain.Product;
import com.romantsev.services.OrderService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Class implementation for orders of user as administrator or client
 */
@Service
public class OrderServiceImpl implements OrderService {
    /**
     * Holds data access object service for interact user with database
     */
    private OrderDao orderDao;

    /**
     * Construct object of service order with dependency injection of orderDao
     *
     * @param orderDao is for injecting dependency
     */
    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * Adds product to cart by productId value
     *
     * @param clientCurrentId provides clientId to verify credentials
     * @param productId       to hand-over exact product from product database to client's cart
     * @return success confirmation result
     */
    @Override
    public boolean addProductToCartById(long clientCurrentId, long productId) {
        return orderDao.addProductToCartById(clientCurrentId, productId);
    }

    /**
     * Removes product from client's cart by productId
     *
     * @param clientCurrentId provides clientId to verify credentials
     * @param productId       to hand-over which product must be removed from client's cart
     * @return success confirmation result
     */
    @Override
    public boolean removeProductFromCartById(long clientCurrentId, long productId) {
        return orderDao.removeProductFromCartById(clientCurrentId, productId);
    }

    /**
     * Allows to get all products from exact client's cart
     *
     * @param clientCurrentId provides provides clientId to verify credentials and retunt it's order
     * @return all products from cart of client pointed by clientId
     */
    @Override
    public Map<Product, Integer> getProductsFromCart(long clientCurrentId) {
        return orderDao.getProductsFromCart(clientCurrentId);
    }

    /**
     * Allows to get all orders of all clients
     *
     * @return database of all client's orders
     */
    @Override
    public Map<Long, Map<Product, Integer>> getOrdersOfAllClients() {
        return orderDao.getOrdersOfAllClients();
    }
}
