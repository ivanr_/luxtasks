package com.romantsev.services.impl;

import com.romantsev.dao.ClientDao;
import com.romantsev.domain.Client;
import com.romantsev.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Implementation api for interaction between data access objects and viewers
 */
@Service
public class ClientServiceImpl implements ClientService {
    /**
     * Holds data access objects to get transaction to database from viewers
     */
    private ClientDao clientDao;

    /**
     * Constructs class providing DI through creating new class object
     *
     * @param clientDao to implements dependency injection with constructor
     */
    @Autowired
    public ClientServiceImpl(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    /**
     * Adds client to clients database
     *
     * @param client - class, that holds all client's data to be added
     * @return success result of client adding
     */
    @Override
    public boolean addClient(Client client) {
        return clientDao.addClient(client);
    }

    /**
     * Deletes client by it's provided id from administrator or client viewer
     *
     * @param clientId provides value for exact client to delete from database
     * @return operation success result
     */
    @Override
    public boolean deleteClientById(long clientId) {
        return clientDao.removeClientById(clientId);
    }

    /**
     * Allows to get pointed client by it's id
     *
     * @param id - client id to find it's data at database
     * @return operation success result
     */
    @Override
    public Client getClientData(long id) {
        return clientDao.getClientData(id);
    }

    /**
     * Modifies client by it's provided id
     *
     * @param client- client id to find it's data at database
     * @return operation success result
     */
    @Override
    public boolean modifyClientById(Client client) {
        return clientDao.modifyClientById(client);
    }

    /**
     * Allows to get all client from database
     *
     * @return hashMap type database with all clients
     */
    @Override
    public Map<Long, Client> getAllClients() {
        return clientDao.getAllClients();
    }
}
