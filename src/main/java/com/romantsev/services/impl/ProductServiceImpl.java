package com.romantsev.services.impl;

import com.romantsev.dao.ProductDao;
import com.romantsev.domain.Product;
import com.romantsev.services.ProductService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

/**
 * This class implements productService to provide user interaction of client or administrator and product database
 */
@Service
public class ProductServiceImpl implements ProductService {
    /**
     * Holds productDao accepted with dependency injection through constructor to get into database
     */
    private ProductDao productDao;

    /**
     * Construct an object with productDao injection
     *
     * @param productDao holds link to get access to database
     */
    public ProductServiceImpl(ProductDao productDao) {
        this.productDao = productDao;
    }

    /**
     * Adds product to list of product into productDb by accepted name and price of product
     *
     * @param name  holds name of exact product
     * @param price holds current price of this product
     * @return success result of product adding
     */
    @Override
    public boolean addProductToList(String name, BigDecimal price) {
        return productDao.addProduct(name, price);
    }

    /**
     * Modifies product's data by provided product class object
     *
     * @param product provides product to modify
     * @return success result of product modifying
     */
    @Override
    public boolean modifyProduct(Product product) {
        return productDao.modifyProduct(product);
    }

    /**
     * Removes product from database by provided productId
     *
     * @param id provides productId to remove
     * @return success result of product removing
     */
    @Override
    public boolean removeProductFromListById(long id) {
        return productDao.removeProductById(id);
    }

    /**
     * Provides database with all products
     *
     * @return Map type of product database
     */
    @Override
    public Map<Long, Product> getAllProducts() {
        return productDao.getAllProducts();
    }
}
