package com.romantsev.services.impl;

import com.romantsev.domain.Client;
import com.romantsev.domain.Product;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ClientService;
import com.romantsev.services.ProductService;
import com.romantsev.services.ValidationService;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class ValidationServiceImpl implements ValidationService {
    private ProductService productService;
    private ClientService clientService;

    public ValidationServiceImpl(ProductService productService, ClientService clientService) {
        this.productService = productService;
        this.clientService = clientService;
    }

    @Override
    public void verifyClientWhileCreating(Client client) {
        validatePhone(client);
        checkPhoneMatchFromDb(filtratePhoneSymbols(client));
        validateAge(client);
        validateEmail(client);
    }

    @Override
    public void verifyProductCreating(Product product) {
        if (hasIdenticalProductName(product, productService)) {
            throw new BusinessException("Product name already exist");
        }
    }

    @Override
    public boolean verifyClientSession(Client client, long id) {
        return client.getId() == id;
    }

    public Client filtratePhoneSymbols(Client clientToAdd) {
        clientToAdd.setPhone(clientToAdd.getPhone().replaceAll("[()-]", ""));
        return clientToAdd;
    }

    private void validateAge(Client client) {
        int maxAge = 200;
        int minAge = 18;
        if (client.getAge() < minAge || client.getAge() > maxAge) {
            throw new BusinessException("Client age must be from " + minAge + " to " + maxAge);
        }
    }

    private void validateEmail(Client client) {
        String regexEmail = "[a-zA-Z]\\w{0,29}@\\w{1,14}.[a-zA-Z]\\w{1,13}";
        boolean isMatch = Pattern.matches(regexEmail, client.getEmail());
        if (!isMatch) {
            throw new BusinessException("Inputed email not valid, try mail@example.com");
        }
    }

    private void validatePhone(Client client) {
        String regexPhone = "[+]38[(]?0\\d{2}[)]?\\d{3}-*\\d{2}-*\\d{2}";
        boolean isMatch = Pattern.matches(regexPhone, client.getPhone());
        if (!isMatch) {
            throw new BusinessException("inputed phone number not match to pattern");
        }
    }

    private void checkPhoneMatchFromDb(Client inputClient) {
        if (clientService.getAllClients() != null) {
            clientService
                    .getAllClients()
                    .forEach((aLong, client) -> {
                        if (client.getPhone().equals(inputClient.getPhone())) {
                            throw new BusinessException("Phone already exist");
                        }
                    });
        }
    }

    private boolean hasIdenticalProductName(Product inputProduct, ProductService service) {
        if (service.getAllProducts() == null) return false;
        return service.getAllProducts()
                .values()
                .stream()
                .map(Product::getName)
                .anyMatch(s -> s.equals(inputProduct.getName()));
    }
}
