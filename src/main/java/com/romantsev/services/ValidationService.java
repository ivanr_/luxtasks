package com.romantsev.services;

import com.romantsev.domain.Client;
import com.romantsev.domain.Product;
import com.romantsev.exceptions.BusinessException;

/**
 * This class provides methods for client and product validation
 */
public interface ValidationService {

    /**
     * Checks for fail while validate client during creating process
     *
     * @param client accepts client to validate
     * @throws BusinessException if validation appear
     */
    void verifyClientWhileCreating(Client client) throws BusinessException;

    /**
     * Filtrates phone symbols for restricted
     *
     * @param clientToAdd is accepted client with filtrated phone
     * @return client with filtrated phone
     */
    Client filtratePhoneSymbols(Client clientToAdd);

    /**
     * Verifies product while creating new for adding to database
     *
     * @param product accepts product to verify
     */
    void verifyProductCreating(Product product);

    /**
     * Verifies client session for allowed client id to exclude wrong user interactions
     *
     * @param client is accepted for check it's session
     * @param id     is a current clientId to verify session
     * @return approve of client actions to be valid
     */
    boolean verifyClientSession(Client client, long id);
}
