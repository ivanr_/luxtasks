package com.romantsev.services;

import com.romantsev.domain.Product;

import java.util.Map;

/**
 * Class for interacting user as administrator or client with personnel orders.
 */
public interface OrderService {

    /**
     * Adds product to client's cart from client or administrator to get an order
     *
     * @param clientCurrentId provides clientId to verify credentials
     * @param productId       to hand-over exact product from product database to client's cart
     * @return success confirmation result
     */
    boolean addProductToCartById(long clientCurrentId, long productId);

    /**
     * Removes product from cart using productId parameter
     *
     * @param clientCurrentId provides clientId to verify credentials
     * @param productId       to hand-over which product must be removed from client's cart
     * @return success confirmation result
     */
    boolean removeProductFromCartById(long clientCurrentId, long productId);

    /**
     * Allows to get products from cart of exact user
     *
     * @param clientCurrentId provides provides clientId to verify credentials and retunt it's order
     * @return whole client cart with type of product and it's numbers
     */
    Map<Product, Integer> getProductsFromCart(long clientCurrentId);

    /**
     * Allows to get all orders of all clients
     *
     * @return database of all client's orders
     */
    Map<Long, Map<Product, Integer>> getOrdersOfAllClients();
}
