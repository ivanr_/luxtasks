package com.romantsev.services;

import com.romantsev.domain.Client;

import java.util.Map;

/**
 * Api for interaction between data access objects and viewers
 */
public interface ClientService {

    /**
     * Adds client to client's database with next options:
     *
     * @param client - class, that holds all client's data to be added
     * @return - parameter which tells about success of adding
     */
    boolean addClient(Client client);

    /**
     * Remove client from database by it's id
     * @param id - accepted client id value to use for client removing
     * @return - parameter which tell about success of removing
     */
    boolean deleteClientById(long id);

    /**
     * Returns client's data using it's provided id
     * @param id - client id to find it's data at database
     * @return - class of client with all client's data
     */
    Client getClientData(long id);

    /**
     * Modify client's data using it's provided id
     * @param client- client id to find it's data at database
     * @return - class of client with all client's data
     */
    boolean modifyClientById(Client client);

    /**
     * Returns all client's data using form database
     * @return - class that contains all data of clients from database
     */
    Map<Long, Client> getAllClients();
}
