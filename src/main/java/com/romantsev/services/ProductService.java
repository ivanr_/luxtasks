package com.romantsev.services;

import com.romantsev.domain.Product;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Class provides service to interact with product sphere
 */
public interface ProductService {

    /**
     * Adds product to database of products
     *
     * @param name  holds name of exact product
     * @param price holds current price of this product
     * @return success result of adding the product
     */
    boolean addProductToList(String name, BigDecimal price);

    /**
     * Modifies existing product from database
     *
     * @param product provides product to add
     * @return success result of adding the product
     */
    boolean modifyProduct(Product product);

    /**
     * Removes product from database by it's id
     *
     * @param id provides productId to add
     * @return success result of adding the product
     */
    boolean removeProductFromListById(long id);

    /**
     * Gets all products from database
     *
     * @return all products from database
     */
    Map<Long, Product> getAllProducts();
}
