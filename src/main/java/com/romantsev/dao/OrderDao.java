package com.romantsev.dao;

import com.romantsev.domain.Product;

import java.util.Map;

public interface OrderDao {
    boolean addProductToCartById(long clientCurrentId, long productId);

    boolean removeProductFromCartById(long clientCurrentId, long productId);

    Map<Product, Integer> getProductsFromCart(long clientCurrentId);

    Map<Long, Map<Product, Integer>> getOrdersOfAllClients();
}
