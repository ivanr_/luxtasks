package com.romantsev.dao;

import com.romantsev.domain.Client;

import java.util.Map;

public interface ClientDao {
    boolean addClient(Client client);

    Client getClientData(long id);

    boolean modifyClientById(Client client);

    boolean removeClientById(long clientId);

    Map<Long, Client> getAllClients();
}
