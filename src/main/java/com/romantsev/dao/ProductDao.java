package com.romantsev.dao;

import com.romantsev.domain.Product;

import java.math.BigDecimal;
import java.util.Map;

public interface ProductDao {
    boolean addProduct(String name, BigDecimal price);

    boolean modifyProduct(Product product);

    boolean removeProductById(long id);

    Map<Long, Product> getAllProducts();
}
