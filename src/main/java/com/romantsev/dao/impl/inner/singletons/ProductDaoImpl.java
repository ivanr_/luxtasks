package com.romantsev.dao.impl.inner.singletons;

import com.romantsev.dao.ProductDao;
import com.romantsev.db.inner.singletons.ProductsDb;
import com.romantsev.domain.Product;

import java.math.BigDecimal;
import java.util.Map;

public class ProductDaoImpl implements ProductDao {

    @Override
    public boolean addProduct(String name, BigDecimal price) {
        return ProductsDb.getInstance().addProductByNamePrice(name, price);
    }

    @Override
    public boolean modifyProduct(Product product) {
        return ProductsDb.getInstance().addProduct(product);
    }

    @Override
    public boolean removeProductById(long id) {
        return ProductsDb.getInstance().removeProductById(id);
    }

    @Override
    public Map<Long, Product> getAllProducts() {
        return ProductsDb.getInstance().getAllProducts();
    }
}
