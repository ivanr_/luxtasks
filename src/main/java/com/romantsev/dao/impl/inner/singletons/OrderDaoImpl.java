package com.romantsev.dao.impl.inner.singletons;

import com.romantsev.dao.OrderDao;
import com.romantsev.db.inner.singletons.ActualOrdersDb;
import com.romantsev.domain.Product;

import java.util.Map;

public class OrderDaoImpl implements OrderDao {
    @Override
    public boolean addProductToCartById(long clientCurrentId, long productId) {
        return ActualOrdersDb.getInstance().addProductToCartById(clientCurrentId, productId);
    }

    @Override
    public boolean removeProductFromCartById(long clientCurrentId, long productId) {
        return ActualOrdersDb.getInstance().removeProductById(clientCurrentId, productId);
    }

    @Override
    public Map<Product, Integer> getProductsFromCart(long clientCurrentId) {
        return ActualOrdersDb.getInstance().getAllProductsFromCart(clientCurrentId);
    }

    @Override
    public Map<Long, Map<Product, Integer>> getOrdersOfAllClients() {
        return ActualOrdersDb.getInstance().getAllClientsOrdersDb();
    }
}
