package com.romantsev.dao.impl.inner.singletons;

import com.romantsev.dao.ClientDao;
import com.romantsev.db.inner.singletons.ClientsDb;
import com.romantsev.domain.Client;

import java.util.Map;

public class ClientDaoImpl implements ClientDao {

    @Override
    public boolean addClient(Client client) {
        return ClientsDb.getInstance().addClient(client);
    }

    @Override
    public Map<Long, Client> getAllClients() {
        return ClientsDb.getInstance().getAllClients();
    }

    @Override
    public Client getClientData(long id) {
        return ClientsDb.getInstance().getClientById(id);
    }

    @Override
    public boolean modifyClientById(Client client) {
        return ClientsDb.getInstance().modifyClient(client);
    }

    @Override
    public boolean removeClientById(long clientId) {
        return ClientsDb.getInstance().removeClientById(clientId);
    }
}
