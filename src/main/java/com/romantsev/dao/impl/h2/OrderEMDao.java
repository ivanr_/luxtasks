package com.romantsev.dao.impl.h2;

import com.romantsev.dao.OrderDao;
import com.romantsev.domain.Orders;
import com.romantsev.domain.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OrderEMDao implements OrderDao {

    private EntityManager entityManager;

    public OrderEMDao() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public boolean addProductToCartById(long clientCurrentId, long productId) {
        entityManager.getTransaction().begin();
        Query query;
        int currentProductNumber;
        query = entityManager
                .createQuery(
                        "SELECT productNumber FROM Orders " +
                                "WHERE clientId=:clientId AND productId=:productId",
                        Integer.class)
                .setParameter("clientId", clientCurrentId)
                .setParameter("productId", productId);
        boolean operationResult;
        if (entityManager.find(Product.class, productId) == null){
            entityManager.getTransaction().commit();
            return false;
        }
        try {
            currentProductNumber = (int) query.getSingleResult();
            System.out.println("Current product quantity: " + currentProductNumber++);
            operationResult = entityManager
                    .createQuery(
                            "UPDATE Orders SET productNumber=:productNumber " +
                                    "WHERE clientId=:clientId AND productId=:productId")
                    .setParameter("productNumber", currentProductNumber)
                    .setParameter("clientId", clientCurrentId)
                    .setParameter("productId", productId)
                    .executeUpdate() == 1;
            System.out.println("RESULT OPERATION: " + operationResult);
            entityManager.getTransaction().commit();
        } catch (NoResultException e) {
            System.out.println("Current product was absent for client in cart");
            entityManager.persist(new Orders(clientCurrentId, productId, 1));
            operationResult = true;
            entityManager.getTransaction().commit();
        }
        entityManager.clear();
        return operationResult;
    }

    @Override
    public boolean removeProductFromCartById(long clientCurrentId, long productId) {
        entityManager.getTransaction().begin();
        boolean operationResult = entityManager
                .createQuery("DELETE FROM Orders WHERE clientId=:clientId AND productId=:productId")
                .setParameter("clientId", clientCurrentId)
                .setParameter("productId", productId)
                .executeUpdate() == 1;
        entityManager.getTransaction().commit();
        entityManager.clear();
        return operationResult;
    }

    @Override
    public Map<Product, Integer> getProductsFromCart(long clientCurrentId) {
        return getOrdersOfAllClients().get(clientCurrentId);
    }

    @Override
    public Map<Long, Map<Product, Integer>> getOrdersOfAllClients() {
        entityManager.getTransaction().begin();
        List<Orders> resultOrdersList = entityManager
                .createQuery("from Orders", Orders.class)
                .getResultList();
        Map<Long, Map<Product, Integer>> resultOrdersMap = new HashMap<>();
        resultOrdersList.forEach(orders -> {
            Product productFromDb = entityManager.find(Product.class, orders.getProductId());
            Map<Product, Integer> tempMap;
            if (resultOrdersMap.containsKey(orders.getClientId())) {
                tempMap = resultOrdersMap.get(orders.getClientId());
            } else {
                tempMap = new HashMap<>();
            }
            tempMap.put(productFromDb, orders.getProductNumber());
            resultOrdersMap.put(orders.getClientId(), tempMap);
        });
        entityManager.getTransaction().commit();
        return resultOrdersMap;
    }
}
