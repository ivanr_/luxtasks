package com.romantsev.dao.impl.h2;

import com.romantsev.dao.ClientDao;
import com.romantsev.domain.Client;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ClientEMDao implements ClientDao {
    private EntityManager entityManager;

    public ClientEMDao() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public boolean addClient(Client client) {
        entityManager.getTransaction().begin();
        entityManager.persist(client);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public Client getClientData(long id) {
        entityManager.getTransaction().begin();
        Client resultClient = entityManager.find(Client.class, id);
        entityManager.getTransaction().commit();
        return resultClient;
    }

    @Override
    public boolean modifyClientById(Client client) {
        entityManager.getTransaction().begin();
        int resultValue = entityManager
                .createQuery(
                        "UPDATE Client " +
                                "SET name=:name, surName=:surname, age=:age, phone=:phone, email=:email " +
                                "WHERE id=:id")
                .setParameter("name", client.getName())
                .setParameter("surname", client.getSurName())
                .setParameter("age", client.getAge())
                .setParameter("phone", client.getPhone())
                .setParameter("email", client.getEmail())
                .setParameter("id", client.getId())
                .executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return resultValue == 1;
    }

    @Override
    public boolean removeClientById(long clientId) {
        entityManager.getTransaction().begin();
        int resultUpdate = entityManager
                .createQuery("DELETE FROM Client WHERE id=:id")
                .setParameter("id", clientId)
                .executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return resultUpdate == 1;
    }

    @Override
    public Map<Long, Client> getAllClients() {
        entityManager.getTransaction().begin();
        entityManager.getEntityManagerFactory().getCache().evictAll();
        List<Client> resultClientsList = entityManager.createQuery("from Client", Client.class).getResultList();
        entityManager.getTransaction().commit();
        Map<Long, Client> resultClientsMap = new HashMap<>();
        resultClientsList.forEach(client -> resultClientsMap.put(client.getId(), client));
        return resultClientsMap;
    }
}
