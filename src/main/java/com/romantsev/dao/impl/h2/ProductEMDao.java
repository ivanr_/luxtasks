package com.romantsev.dao.impl.h2;

import com.romantsev.dao.OrderDao;
import com.romantsev.dao.ProductDao;
import com.romantsev.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProductEMDao implements ProductDao {

    private final EntityManager entityManager;

    private final OrderDao orderDao;

    @Autowired
    public ProductEMDao(OrderDao orderDao) {
        this.orderDao = orderDao;
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence");
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    public boolean addProduct(String name, BigDecimal price) {
        Product productToAdd = new Product(name, price);
        entityManager.getTransaction().begin();
        entityManager.persist(productToAdd);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean modifyProduct(Product product) {
        entityManager.getTransaction().begin();
        int resultValue = entityManager
                .createQuery(
                        "UPDATE Product " +
                                "SET name=:name, price=:price " +
                                "WHERE id=:id")
                .setParameter("name", product.getName())
                .setParameter("price", product.getPrice())
                .setParameter("id", product.getId())
                .executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return resultValue > 0;
    }

    @Override
    public boolean removeProductById(long id) {
        entityManager.getTransaction().begin();
        int resultUpdate = entityManager
                .createQuery("DELETE FROM Product WHERE id=:id")
                .setParameter("id", id)
                .executeUpdate();
        List<Long> clientIds = getAllClientIds(entityManager);
        clientIds.forEach(clientId -> orderDao.removeProductFromCartById(clientId, id));
        entityManager.flush();
        entityManager.getTransaction().commit();
        entityManager.clear();
        return resultUpdate == 1;
    }

    private List<Long> getAllClientIds(EntityManager entityManager) {
        return entityManager
                .createQuery(
                        "SELECT id FROM Client", Long.class)
                .getResultList();
    }

    @Override
    public Map<Long, Product> getAllProducts() {
        entityManager.getTransaction().begin();
        List<Product> resultProductsList = entityManager
                .createQuery("from Product", Product.class)
                .getResultList();
        entityManager.getTransaction().commit();
        Map<Long, Product> resultProductsMap = new HashMap<>();
        resultProductsList.forEach(product -> resultProductsMap.put(product.getId(), product));
        return resultProductsMap;
    }


}
