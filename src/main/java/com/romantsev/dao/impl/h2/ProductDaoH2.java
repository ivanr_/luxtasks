package com.romantsev.dao.impl.h2;

import com.romantsev.dao.OrderDao;
import com.romantsev.dao.ProductDao;
import com.romantsev.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Repository
public class ProductDaoH2 implements ProductDao {
    private static String LOGIN = "admin";
    private static String PASSWORD = "admin";
    private static String JDBC_PATH =
            "jdbc:h2:tcp://localhost/" +
                    System.getProperty("user.dir") +
                    "/src/main/java/com/romantsev/db/h2/h2testdb";

    private OrderDao orderDao;

    @Autowired
    public ProductDaoH2(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    ProductDaoH2(OrderDao orderDao, String JDBC_PATH, String LOGIN, String PASSWORD) {
        this.orderDao = orderDao;
        ProductDaoH2.LOGIN = LOGIN;
        ProductDaoH2.PASSWORD = PASSWORD;
        ProductDaoH2.JDBC_PATH = JDBC_PATH;
    }

    @Override
    public boolean addProduct(String name, BigDecimal price) {
        String request =
                "INSERT INTO PRODUCT (NAME, PRICE)" +
                        "VALUES (?, ?)";
        return sendH2RequestWithProduct(new Product(name, price), request);
    }

    @Override
    public boolean modifyProduct(Product product) {
        String request =
                "UPDATE PRODUCT " +
                        "SET NAME = ?, PRICE = ? " +
                        "WHERE ID=" + product.getId();
        return sendH2RequestWithProduct(product, request);
    }

    @Override
    public boolean removeProductById(long id) {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "DELETE FROM PRODUCT WHERE ID=?";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            return deleteProductFromAllCarts(id);
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return false;
    }

    private boolean deleteProductFromAllCarts(long productId) {
        List<Long> clientsId = getAllClientsId();
        clientsId.forEach(clientId -> orderDao.removeProductFromCartById(clientId, productId));
        return true;
    }

    private List<Long> getAllClientsId() {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT ID FROM CLIENT";
            PreparedStatement statement = connection.prepareStatement(request);
            ResultSet resultSet = statement.executeQuery();
            List<Long> clientsId = new ArrayList<>();
            while (resultSet.next()) {
                clientsId.add(resultSet.getLong(1));
            }
            resultSet.close();
            statement.close();
            return clientsId;
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public Map<Long, Product> getAllProducts() {
        Map<Long, Product> productDb = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT * FROM PRODUCT ";
            PreparedStatement statement = connection.prepareStatement(request);
            ResultSet resultSet = statement.executeQuery();
            productDb = new HashMap<>();
            while (resultSet.next()) {
                Long productId = resultSet.getLong(1);
                String name = resultSet.getString(2);
                BigDecimal price = resultSet.getBigDecimal(3);
                Product currentProduct = new Product(productId, name, price);
                productDb.put(productId, currentProduct);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return productDb;
    }

    private boolean sendH2RequestWithProduct(Product product, String request) {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setString(1, product.getName());
            statement.setBigDecimal(2, product.getPrice());
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return false;
    }
}
