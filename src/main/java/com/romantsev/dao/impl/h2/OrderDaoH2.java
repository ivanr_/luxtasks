package com.romantsev.dao.impl.h2;

import com.romantsev.dao.OrderDao;
import com.romantsev.domain.Product;
import org.h2.jdbc.JdbcSQLNonTransientException;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

//@Repository
public class OrderDaoH2 implements OrderDao {
    private String LOGIN = "admin";
    private String PASSWORD = "admin";
    private String JDBC_PATH =
            "jdbc:h2:tcp://localhost/" +
                    System.getProperty("user.dir") +
                    "/src/main/java/com/romantsev/db/h2/h2testdb";

    OrderDaoH2(String JDBC_PATH, String LOGIN, String PASSWORD) {
        this.LOGIN = LOGIN;
        this.PASSWORD = PASSWORD;
        this.JDBC_PATH = JDBC_PATH;
    }

    public OrderDaoH2() {
    }

    @Override
    public boolean addProductToCartById(long clientCurrentId, long productId) {
        int currentProductCount;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT PRODUCTNUMBER FROM ORDERS WHERE CLIENTID=? AND PRODUCTID=?";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, clientCurrentId);
            statement.setLong(2, productId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            try {
                currentProductCount = resultSet.getInt(1);
                System.out.println("Current product quantity: " + currentProductCount++);
                request = "UPDATE ORDERS SET PRODUCTNUMBER=? WHERE CLIENTID=? AND PRODUCTID=" + productId;
                statement = connection.prepareStatement(request);
                statement.setInt(1, currentProductCount);
                statement.setLong(2, clientCurrentId);
//                statement.setLong(3, productId);
            } catch (JdbcSQLNonTransientException e) {
                currentProductCount = 1;
                System.out.println("current product is absent for client in cart");
                request = "INSERT INTO ORDERS (CLIENTID, PRODUCTID, PRODUCTNUMBER)" +
                        " VALUES (?, ?, ?)";
                statement = connection.prepareStatement(request);
                statement.setLong(1, clientCurrentId);
                statement.setLong(2, productId);
                statement.setLong(3, currentProductCount);
            }
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean removeProductFromCartById(long clientCurrentId, long productId) {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "DELETE FROM ORDERS WHERE CLIENTID=? AND PRODUCTID=?";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, clientCurrentId);
            statement.setLong(2, productId);
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Map<Product, Integer> getProductsFromCart(long clientCurrentId) {
        Map<Long, Map<Product, Integer>> ordersDb = getOrdersOfAllClients();
        return ordersDb.get(clientCurrentId);
    }

    @Override
    public Map<Long, Map<Product, Integer>> getOrdersOfAllClients() {
        Map<Long, Map<Product, Integer>> ordersDb = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT * FROM ORDERS ";
            PreparedStatement statement = connection.prepareStatement(request);
            ResultSet resultSet = statement.executeQuery();
            ordersDb = new HashMap<>();
            while (resultSet.next()) {
                long clientId = resultSet.getLong(2);
                long productId = resultSet.getLong(3);
                int count = resultSet.getInt(4);
                Product currentProduct = getProductById(productId);
                if (!ordersDb.containsKey(clientId)) {
                    Map<Product, Integer> tempMap = new HashMap<>();
                    tempMap.put(currentProduct, count);
                    ordersDb.put(clientId, tempMap);
                } else {
                    Map<Product, Integer> tempMap = ordersDb.get(clientId);
                    tempMap.put(currentProduct, count);
                    ordersDb.put(clientId, tempMap);
                }
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return ordersDb;
    }

    private Product getProductById(long productId) {
        Product currentProduct = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT * FROM PRODUCT WHERE ID = ?";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, productId);
            ResultSet resultSet = statement.executeQuery();
            currentProduct = new Product();
            while (resultSet.next()) {
                long productIdFromDb = resultSet.getLong(1);
                String name = resultSet.getString(2);
                BigDecimal price = resultSet.getBigDecimal(3);
                currentProduct = new Product(productIdFromDb, name, price);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return currentProduct;
    }
}
