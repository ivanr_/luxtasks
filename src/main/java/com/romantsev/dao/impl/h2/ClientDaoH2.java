package com.romantsev.dao.impl.h2;

import com.romantsev.dao.ClientDao;
import com.romantsev.domain.Client;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

//@Repository
public class ClientDaoH2 implements ClientDao {

    private static String LOGIN = "admin";
    private static String PASSWORD = "admin";
    private static String JDBC_PATH =
            "jdbc:h2:tcp://localhost/" +
                    System.getProperty("user.dir") +
                    "/src/main/java/com/romantsev/db/h2/h2testdb";
//                    "/luxshop/src/main/java/com/romantsev/db/h2/h2testdb";

    public ClientDaoH2() {
    }

    public ClientDaoH2(String JDBC_PATH, String LOGIN, String PASSWORD) {
        ClientDaoH2.LOGIN = LOGIN;
        ClientDaoH2.PASSWORD = PASSWORD;
        ClientDaoH2.JDBC_PATH = JDBC_PATH;
    }

    @Override
    public boolean addClient(Client client) {
        String request =
                "INSERT INTO CLIENT (NAME, SURNAME, AGE, PHONE, EMAIL)" +
                        "VALUES (?, ?, ?, ?, ?)";
        return sendH2RequestWithClients(client, request);
    }

    @Override
    public Client getClientData(long id) {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM CLIENT WHERE ID=?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            String name = resultSet.getString("NAME");
            String surname = resultSet.getString("SURNAME");
            int age = resultSet.getInt("AGE");
            String phone = resultSet.getString("PHONE");
            String email = resultSet.getString("EMAIL");
            resultSet.close();
            statement.close();
            return new Client(id, name, surname, age, email, phone);
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean modifyClientById(Client client) {
        String request =
                "UPDATE CLIENT " +
                        "SET NAME = ?, SURNAME = ?, AGE = ?, PHONE = ?, EMAIL = ?" +
                        "WHERE ID=" + client.getId();
        return sendH2RequestWithClients(client, request);
    }

    @Override
    public boolean removeClientById(long clientId) {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "DELETE FROM CLIENT WHERE ID=?";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, clientId);
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Map<Long, Client> getAllClients() {
        Map<Long, Client> clientDb = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT * FROM CLIENT ";
            PreparedStatement statement = connection.prepareStatement(request);
            ResultSet resultSet = statement.executeQuery();
            clientDb = new HashMap<>();
            while (resultSet.next()) {
                Long clientId = resultSet.getLong("ID");
                String name = resultSet.getString("NAME");
                String surname = resultSet.getString("SURNAME");
                int age = resultSet.getInt("AGE");
                String phone = resultSet.getString("PHONE");
                String email = resultSet.getString("EMAIL");
                Client currentClient = new Client(clientId, name, surname, age, email, phone);
                clientDb.put(clientId, currentClient);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return clientDb;
    }

    private boolean sendH2RequestWithClients(Client client, String request) {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setString(1, client.getName());
            statement.setString(2, client.getSurName());
            statement.setInt(3, client.getAge());
            statement.setString(4, client.getPhone());
            statement.setString(5, client.getEmail());
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return false;
    }
}
