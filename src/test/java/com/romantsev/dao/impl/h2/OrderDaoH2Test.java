package com.romantsev.dao.impl.h2;

import com.romantsev.dao.OrderDao;
import com.romantsev.db.h2.H2ConnectorTest;
import com.romantsev.domain.Client;
import com.romantsev.domain.Product;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class OrderDaoH2Test {

    private static final String LOGIN = "adminTest";
    private static final String PASSWORD = "adminTest";
    private static final String JDBC_PATH =
            "jdbc:h2:tcp://localhost/" +
                    System.getProperty("user.dir") +
                    "/src/test/java/com/romantsev/db/h2/h2testdb";

    private OrderDao orderDao;
    private Client expectedClient;
    private H2ConnectorTest connectorTest;
    private Product expectedProduct;
    private int expectedProductCount = 3;

    @Before
    public void setUp() throws SQLException {
        connectorTest = new H2ConnectorTest();
        connectorTest.connect();
        orderDao = new OrderDaoH2(JDBC_PATH, LOGIN, PASSWORD);
        expectedClient = new Client("testName", "testSurname", 50, "testMail@testDomain.com", "+380981234567");
        expectedProduct = new Product("testProduct", new BigDecimal(33.0112));
        expectedProduct.setId(1);
    }

    @After
    public void tearDown() {
        connectorTest.disconnect();
        orderDao = null;
        expectedClient = null;
        expectedProduct = null;
    }

    @Test
    public void addProductToCartById() {
//        GIVEN
        addTestProductToOrder();
//        WHEN
        boolean addProductResult = orderDao.addProductToCartById(expectedClient.getId(), expectedProduct.getId());
//        THEN
        Assert.assertTrue(addProductResult);
        System.out.println("Success OrderDaoH2.addProductToCartById() test");
        removeTestProductsFromOrderDb();
    }

    @Test
    public void removeProductFromCartById() {
//        GIVEN
        addTestProductToOrder();
//        WHEN
        boolean expectedRemoveResult = orderDao.removeProductFromCartById(expectedClient.getId(), expectedProduct.getId());
//        THEN
        Assert.assertTrue(expectedRemoveResult);
        System.out.println("Success OrderDaoH2.removeProductFromCartById() test");
    }

    @Test
    public void getProductsFromCart() {
//        GIVEN
        removeTestProductsFromOrderDb();
        long testProductId = addTestProductToDbReturnId();
        expectedProduct.setId(testProductId);
        addTestProductToOrder();
        Map<Product, Integer> expectedProductsFromCart = getExpectedClientOrders();
//        WHEN
        Map<Product, Integer> actualClientOrder = orderDao.getProductsFromCart(expectedClient.getId());

//        THEN
        Assert.assertEquals(expectedProductsFromCart, actualClientOrder);
        System.out.println("Success OrderDaoH2.getProductsFromCart() test");
        removeTestProductsFromOrderDb();
    }

    @Test
    public void getOrdersOfAllClients() {
//        GIVEN
        addTestProductToDbReturnId();
        Map<Long, Map<Product, Integer>> expectedOrderDb = getAllTestOrders();
//        WHEN
        Map<Long, Map<Product, Integer>> actualOrderDb = orderDao.getOrdersOfAllClients();
//        THEN
        Assert.assertEquals(expectedOrderDb,actualOrderDb);
        System.out.println("Success OrderDaoH2.getOrdersOfAllClients() test");
    }

    private Map<Long, Map<Product, Integer>> getAllTestOrders() {
        Map<Long, Map<Product, Integer>> ordersDb = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT * FROM ORDERS ";
            PreparedStatement statement = connection.prepareStatement(request);
            ResultSet resultSet = statement.executeQuery();
            ordersDb = new HashMap<>();
            while (resultSet.next()) {
                long clientId = resultSet.getLong(2);
                long productId = resultSet.getLong(3);
                int count = resultSet.getInt(4);
                Product currentProduct = getProductById(productId);
                if (!ordersDb.containsKey(clientId)) {
                    Map<Product, Integer> tempMap = new HashMap<>();
                    tempMap.put(currentProduct, count);
                    ordersDb.put(clientId, tempMap);
                } else {
                    Map<Product, Integer> tempMap = ordersDb.get(clientId);
                    tempMap.put(currentProduct, count);
                    ordersDb.put(clientId, tempMap);
                }
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return ordersDb;
    }

    private Product getProductById(long productId) {
        Product currentProduct = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT * FROM PRODUCT WHERE ID = ?";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, productId);
            ResultSet resultSet = statement.executeQuery();
            currentProduct = new Product();
            while (resultSet.next()) {
                long productIdFromDb = resultSet.getLong(1);
                String name = resultSet.getString(2);
                BigDecimal price = resultSet.getBigDecimal(3);
                currentProduct = new Product(productIdFromDb, name, price);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
        return currentProduct;
    }

    private long addTestProductToDbReturnId() {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "INSERT INTO PRODUCT(NAME, PRICE) " +
                    "VALUES (?, ?)";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setString(1, expectedProduct.getName());
            statement.setBigDecimal(2, expectedProduct.getPrice());
            statement.execute();
            request = "SELECT MAX(ID) FROM PRODUCT";
            statement = connection.prepareStatement(request);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getLong(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private Map<Product, Integer> getExpectedClientOrders() {
        Map<Product, Integer> clientOrder = new HashMap<>();
        clientOrder.put(expectedProduct, expectedProductCount);
        return clientOrder;
    }

    private void addTestProductToOrder() {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "INSERT INTO ORDERS(CLIENTID, PRODUCTID, PRODUCTNUMBER) " +
                    "VALUES (?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, expectedClient.getId());
            statement.setLong(2, expectedProduct.getId());
            statement.setInt(3, expectedProductCount);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void removeTestProductsFromOrderDb() {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "DELETE FROM ORDERS WHERE CLIENTID=?";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, expectedClient.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}