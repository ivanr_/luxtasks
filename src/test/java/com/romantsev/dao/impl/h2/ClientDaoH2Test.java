package com.romantsev.dao.impl.h2;

import com.romantsev.dao.ClientDao;
import com.romantsev.db.h2.H2ConnectorTest;
import com.romantsev.domain.Client;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class ClientDaoH2Test {

    private static final String LOGIN = "adminTest";
    private static final String PASSWORD = "adminTest";
    private static final String JDBC_PATH =
            "jdbc:h2:tcp://localhost/" +
                    System.getProperty("user.dir") +
                    "/src/test/java/com/romantsev/db/h2/h2testdb";

    private ClientDao clientDao;

    private H2ConnectorTest connectorTest;

    private Client testClient;

    @Before
    public void setUp() throws Exception {
        connectorTest = new H2ConnectorTest();
        connectorTest.connect();
        clientDao = new ClientDaoH2(JDBC_PATH, LOGIN, PASSWORD);
        testClient = new Client("testName", "testSurname", 100, "testMail@testDomain.com", "+380981234567");
    }

    @After
    public void tearDown() {
        connectorTest.disconnect();
        testClient = null;
    }

    @Test
    public void addClient() {
//        GIVEN
        boolean addClientResult;
//        WHEN
        addClientResult = clientDao.addClient(testClient);
//        THEN
        Assert.assertTrue(addClientResult);
        System.out.println("Success ClientDaoH2.addClient() test");
    }

    @Test
    public void getClientData() {
//        GIVEN
        Client clientResponseFromDb = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            int maxClientDbId = getMaxClientIdFromDb(connection);
            addTestClientToDb(connection);
            testClient.setId(++maxClientDbId);
//        WHEN
            clientResponseFromDb = clientDao.getClientData(maxClientDbId);
            removeTestClientById(maxClientDbId, connection);
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
//        THEN
        Assert.assertEquals(testClient, clientResponseFromDb);
        System.out.println("Success ClientDaoH2.getClientData() test");
    }

    @Test
    public void modifyClientById() {
//        GIVEN
        boolean modifyResult = false;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            addTestClientToDb(connection);
            int lastClientId = getMaxClientIdFromDb(connection);
            testClient.setName("modifiedTestName");
            testClient.setId(lastClientId);
//        WHEN
            modifyResult = clientDao.modifyClientById(testClient);
            removeTestClientById(lastClientId, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        THEN
        Assert.assertTrue(modifyResult);
        System.out.println("Success ClientDaoH2.modifyClientById() test");
    }

    @Test
    public void removeClientById() {
//        GIVEN
        boolean removeResult = false;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            addTestClientToDb(connection);
            int maxClientDbId = getMaxClientIdFromDb(connection);
//        WHEN
            removeResult = clientDao.removeClientById(maxClientDbId);
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
//        THEN
        Assert.assertTrue(removeResult);
        System.out.println("Success ClientDaoH2.removeClientById() test");
    }

    @Test
    public void getAllClients() {
//        GIVEN
        Map<Long, Client> expectedClientsTestDb = null;
        Map<Long, Client> actualClientsTestDb = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            expectedClientsTestDb = getAllClientsFromTestDb(connection);
//        WHEN
            actualClientsTestDb = clientDao.getAllClients();
        } catch (SQLException e) {
            System.out.println("Db connection problem");
            e.printStackTrace();
        }
//        THEN
        Assert.assertEquals(expectedClientsTestDb, actualClientsTestDb);
        System.out.println("Success ClientDaoH2.getAllClients() test");
    }

    private Map<Long, Client> getAllClientsFromTestDb(Connection connection) throws SQLException {
        String request = "SELECT * FROM CLIENT ";
        PreparedStatement statement = connection.prepareStatement(request);
        ResultSet resultSet = statement.executeQuery();
        Map<Long, Client> clientDb = new HashMap<>();
        while (resultSet.next()) {
            Long clientId = resultSet.getLong("ID");
            Client currentClient = new Client(
                    resultSet.getLong("ID"),
                    resultSet.getString("NAME"),
                    resultSet.getString("SURNAME"),
                    resultSet.getInt("AGE"),
                    resultSet.getString("EMAIL"),
                    resultSet.getString("PHONE"));
            clientDb.put(clientId, currentClient);
        }
        resultSet.close();
        statement.close();
        return clientDb;
    }

    private void removeTestClientById(long clientId, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM CLIENT WHERE ID=?"
        );
        statement.setLong(1, clientId);
        statement.execute();
        statement.close();
    }

    private int getMaxClientIdFromDb(Connection connection) throws SQLException {
        String request = "SELECT MAX(ID) FROM CLIENT";
        PreparedStatement statement = connection.prepareStatement(request);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }

    private void addTestClientToDb(Connection connection) throws SQLException {
        String request;
        PreparedStatement statement;
        request = "INSERT INTO CLIENT(NAME, SURNAME, AGE, EMAIL, PHONE) VALUES (?, ?, ?, ?, ?)";
        statement = connection.prepareStatement(request);
        statement.setString(1, testClient.getName());
        statement.setString(2, testClient.getSurName());
        statement.setInt(3, testClient.getAge());
        statement.setString(4, testClient.getEmail());
        statement.setString(5, testClient.getPhone());
        statement.execute();
        statement.close();
    }
}