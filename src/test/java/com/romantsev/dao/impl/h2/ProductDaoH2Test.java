package com.romantsev.dao.impl.h2;

import com.romantsev.dao.OrderDao;
import com.romantsev.dao.ProductDao;
import com.romantsev.db.h2.H2ConnectorTest;
import com.romantsev.domain.Product;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class ProductDaoH2Test {

    private static final String LOGIN = "adminTest";
    private static final String PASSWORD = "adminTest";
    private static final String JDBC_PATH =
            "jdbc:h2:tcp://localhost/" +
                    System.getProperty("user.dir") +
                    "/src/test/java/com/romantsev/db/h2/h2testdb";

    private ProductDao productDao;

    private H2ConnectorTest connectorTest;

    private Product testProduct;

    private Map<Long, Product> expectedProductsTestDb;

    @Mock
    private OrderDao orderDao;

    @Before
    public void setUp() throws Exception {
        connectorTest = new H2ConnectorTest();
        connectorTest.connect();
        productDao = new ProductDaoH2(orderDao, JDBC_PATH, LOGIN, PASSWORD);
        testProduct = new Product("testProduct", new BigDecimal(123.31));
    }

    @After
    public void tearDown() {
        connectorTest.disconnect();
        testProduct = null;
    }

    @Test
    public void addProduct() {
//        GIVEN
        boolean addProductResult;
//        WHEN
        addProductResult = productDao.addProduct(testProduct.getName(), testProduct.getPrice());
//        THEN
        Assert.assertTrue(addProductResult);
        System.out.println("Success ProductDaoH2.addProduct() test");
        removeTestProductByName();
    }

    @Test
    public void modifyProduct() {
//        GIVEN
        addTestProductToDb();
        boolean modifyResult;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            long productId = getMaxProductIdFromDb(connection);
            testProduct.setName("modifiedTestName");
            testProduct.setId(productId);
//        WHEN
        } catch (SQLException e) {
            e.printStackTrace();
        }
        modifyResult = productDao.modifyProduct(testProduct);
//        THEN
        Assert.assertTrue(modifyResult);
        System.out.println("Success ProductDaoH2.modifyProduct() test");
        removeTestProductByName();
    }

    @Test
    public void removeProductById() {
//        GIVEN
        addTestProductToDb();
        long productId = 0;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            productId = getMaxProductIdFromDb(connection);
//        WHEN
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Mockito.when(orderDao.removeProductFromCartById(Mockito.anyLong(), Mockito.anyLong())).thenReturn(true);
        boolean removeProductResult = productDao.removeProductById(productId);
//        THEN
        Assert.assertTrue(removeProductResult);
        System.out.println("Success ProductDaoH2.removeProductById() test");
    }

    @Test
    public void getAllProducts() {
//        GIVEN
        addProductsIntoTestDb();
        expectedProductsTestDb = getProductsFromTestDb();
//        WHEN
        Map<Long, Product> actualProductsTestDb = productDao.getAllProducts();
//        THEN
        Assert.assertEquals(expectedProductsTestDb, actualProductsTestDb);
        System.out.println("Success ProductDaoH2.getAllProducts() test");
        expectedProductsTestDb = null;
        removeTestProductByName();
    }

    private Map<Long, Product> getProductsFromTestDb() {
        Map<Long, Product> productDb = null;
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "SELECT * FROM PRODUCT ";
            PreparedStatement statement = connection.prepareStatement(request);
            ResultSet resultSet = statement.executeQuery();
            productDb = new HashMap<>();
            while (resultSet.next()) {
                Long productId = resultSet.getLong("ID");
                Product currentProduct = new Product(
                        productId,
                        resultSet.getString("NAME"),
                        resultSet.getBigDecimal("PRICE"));
                productDb.put(productId, currentProduct);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productDb;
    }

    private void addProductsIntoTestDb() {
        expectedProductsTestDb = new HashMap<>();
        expectedProductsTestDb.put(0L, testProduct);
        testProduct.setName("productname2");
        expectedProductsTestDb.put(1L, testProduct);
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            for (Product product : expectedProductsTestDb.values()) {
                String request = "INSERT INTO PRODUCT(NAME, PRICE) VALUES (?, ?)";
                PreparedStatement statement = connection.prepareStatement(request);
                statement.setString(1, product.getName());
                statement.setBigDecimal(2, product.getPrice());
                statement.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private long getMaxProductIdFromDb(Connection connection) throws SQLException {
        String request = "SELECT MAX(ID) FROM PRODUCT";
        PreparedStatement statement = connection.prepareStatement(request);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getLong(1);
    }

    private void removeTestProductByName() {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            String request = "DELETE FROM PRODUCT WHERE NAME=?";
            PreparedStatement statement = connection.prepareStatement(request);
            statement.setString(1, testProduct.getName());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addTestProductToDb() {
        try (Connection connection = DriverManager.getConnection(JDBC_PATH, LOGIN, PASSWORD)) {
            PreparedStatement statement;
            String request = "INSERT INTO PRODUCT(NAME, PRICE) VALUES (?, ?)";
            statement = connection.prepareStatement(request);
            statement.setString(1, testProduct.getName());
            statement.setBigDecimal(2, testProduct.getPrice());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}