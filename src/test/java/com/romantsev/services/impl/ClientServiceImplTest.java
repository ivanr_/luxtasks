package com.romantsev.services.impl;

import com.romantsev.dao.ClientDao;
import com.romantsev.domain.Client;
import com.romantsev.services.ClientService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceImplTest {
    @Mock
    private ClientDao clientDao;

    private ClientService clientService;

    private Client expectedClient;

    @BeforeClass
    public static void initClass() {
        System.out.println("Before Class");
    }

    @Before
    public void init() {
        clientService = new ClientServiceImpl(clientDao);
        expectedClient = new Client();
        expectedClient.setName("TestName");
        expectedClient.setSurName("TestSurName");
        expectedClient.setId(1);
        expectedClient.setAge(1);
        expectedClient.setEmail("testMail@mail.com");
        expectedClient.setPhone("+380(011)100-19-23");
    }

    @After
    public void tearDawn() {
        clientService = null;
        expectedClient = null;
    }

    @AfterClass
    public static void tearDawn1() {
        System.out.println("After Class");
    }

    @Test
    public void addClientTest() {
//        GIVEN
        Mockito.when(clientDao.addClient(expectedClient)).thenReturn(true);
//        WHEN
        boolean operationResult = clientService.addClient(expectedClient);
//        THEN
        Mockito.verify(clientDao).addClient(expectedClient);
        Assert.assertTrue(operationResult);
        System.out.println("Success Service.addClient() test");
    }

    @Test
    public void getClientDataTest() {
//        GIVEN
        Mockito.when(clientDao.getClientData(expectedClient.getId())).thenReturn(expectedClient);
//        WHEN
        Client clientFromDb = clientService.getClientData(expectedClient.getId());
//        THEN
        Assert.assertEquals(clientFromDb, expectedClient);
    }

    @Test
    public void deleteClientById() {
//        GIVEN
        Mockito.when(clientDao.removeClientById(expectedClient.getId())).thenReturn(true);
//        WHEN
        boolean deleteClientByIdResult = clientService.deleteClientById(expectedClient.getId());
//        THEN
        Assert.assertTrue(deleteClientByIdResult);
        System.out.println("Success Service.deleteClientById() test");

    }

    @Test
    public void modifyClientById() {
//        GIVEN
        Mockito.when(clientDao.modifyClientById(expectedClient)).thenReturn(true);
//        WHEN
        boolean modifyByIdResult = clientService.modifyClientById(expectedClient);
//        THEN
        Assert.assertTrue(modifyByIdResult);
        System.out.println("Success Service.modifyClientById() test");
    }

    @Test
    public void getAllClients() {
//        GIVEN
        Map<Long, Client> expectedDb = getExpectedClientsDb();
        Mockito.when(clientDao.getAllClients()).thenReturn(expectedDb);
//        WHEN
        Map<Long, Client> returnedDb = clientService.getAllClients();
//        THEN
        Assert.assertEquals(expectedDb, returnedDb);
        System.out.println("Success ClientService.getAllClient() test");
    }

    private Map<Long, Client> getExpectedClientsDb() {
        Map<Long, Client> expectedDb = new HashMap<>();
        expectedDb.put(expectedClient.getId(), expectedClient);
        expectedClient.setId(expectedClient.getId() + 1);
        expectedClient.setName("Cloned client");
        expectedDb.put(expectedClient.getId(), expectedClient);
        return expectedDb;
    }
}