package com.romantsev.services.impl;

import com.romantsev.dao.OrderDao;
import com.romantsev.domain.Product;
import com.romantsev.services.OrderService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

    private long clientId = 1L;
    private long productId = 2L;
    private OrderService orderService;

    @Mock
    private OrderDao orderDao;

    @Before
    public void setUp() {
        orderService = new OrderServiceImpl(orderDao);
    }

    @After
    public void tearDown() {
        orderService = null;
    }

    @Test
    public void addProductToCartById() {
//        GIVEN
        Mockito.when(orderDao.addProductToCartById(clientId, productId)).thenReturn(true);
//        WHEN
        boolean operationResult = orderService.addProductToCartById(clientId, productId);
//        THEN
        Assert.assertTrue(operationResult);
        System.out.println("Success OrderServiceImpl.addProductToCartById() test");
    }

    @Test
    public void removeProductFromCartById() {
//        GIVEN
        Mockito.when(orderDao.removeProductFromCartById(clientId, productId)).thenReturn(true);
//        WHEN
        boolean operationResult = orderService.removeProductFromCartById(clientId, productId);
//        THEN
        Assert.assertTrue(operationResult);
        System.out.println("Success OrderServiceImpl.removeProductFromCartById() test");
    }

    @Test
    public void getProductsFromCart() {
//        GIVEN
        Map<Product, Integer> expectedClientOrderDb = getClientOrderDb();
        Mockito.when(orderDao.getProductsFromCart(clientId)).thenReturn(expectedClientOrderDb);
//        WHEN
        Map<Product, Integer> actualClientOrder = orderService.getProductsFromCart(clientId);
//        THEN
        Assert.assertEquals(expectedClientOrderDb, actualClientOrder);
        System.out.println("Success OrderServiceImpl.getProductsFromCart() test");
    }

    @Test
    public void getOrdersOfAllClients() {
//        GIVEN
        Map<Long, Map<Product, Integer>> expectedClientsOrders = expectedClientsOrders();
        Mockito.when(orderDao.getOrdersOfAllClients()).thenReturn(expectedClientsOrders);
//        WHEN
        Map<Long, Map<Product, Integer>> actualClientsOrders = orderService.getOrdersOfAllClients();
//        THEN
        Assert.assertEquals(expectedClientsOrders, actualClientsOrders);
        System.out.println("Success OrderServiceImpl.getOrdersOfAllClients() test");
    }

    private Map<Long, Map<Product, Integer>> expectedClientsOrders() {
        Map<Product, Integer> clientOrderDb = getClientOrderDb();
        Map<Long, Map<Product, Integer>> clientsOrders = new HashMap<>();
        clientsOrders.put(clientId, clientOrderDb);
        return clientsOrders;
    }

    private Map<Product, Integer> getClientOrderDb() {
        Map<Product, Integer> clientOrderDb = new HashMap<>();
        Product product = new Product("testProduct", BigDecimal.valueOf(clientId));
        Integer productNumber = 3;
        clientOrderDb.put(product, productNumber);
        return clientOrderDb;
    }
}