package com.romantsev.services.impl;

import com.romantsev.domain.Client;
import com.romantsev.domain.Product;
import com.romantsev.exceptions.BusinessException;
import com.romantsev.services.ClientService;
import com.romantsev.services.ProductService;
import com.romantsev.services.ValidationService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceImplTest {

    private ValidationService validationService;

    private Client expectedClient;

    @Mock
    private ClientService clientService;

    @Mock
    private ProductService productService;

    @Before
    public void setValidationService() {
        validationService = new ValidationServiceImpl(productService, clientService);
        expectedClient = new Client(1,
                "TestName",
                "TestSurName",
                21,
                "testMail@mail.com",
                "+38(011)100-19-30");
    }

    @After
    public void tearDown() {
        validationService = null;
        expectedClient = null;
    }

    @Test
    public void isFailVerificationClientWhileCreating() {
//        GIVEN
        Map<Long, Client> clientDb = new HashMap<>();
        clientDb.put(expectedClient.getId(), expectedClient);
        String changedPhone = changeExpectedClientPhone(expectedClient.getPhone());
        Client clientWithAnotherPhone = new Client("Name", "Surname", 32, "somEmail@email.com", changedPhone);
        Mockito.when(clientService.getAllClients()).thenReturn(clientDb);
//        WHEN
        try {
            validationService.verifyClientWhileCreating(clientWithAnotherPhone);
        } catch (BusinessException e) {
            Assert.fail("Client verification fail, reason:\n" + e.getMessage());
        }
//        THEN
        System.out.println("Success ValidationService.verifyClientWhileCreating() test");
    }

    @Test
    public void verifyProductCreating() {
//        GIVEN
        Map<Long, Product> productDb = new HashMap<>();
        productDb.put(null, new Product("new product", new BigDecimal(0.001)));
        Product expectedProduct = new Product("test product name", new BigDecimal(0.00001));
        Mockito.when(productService.getAllProducts()).thenReturn(productDb);
//        WHEN
//        THEN
        try {
            validationService.verifyProductCreating(expectedProduct);
        } catch (BusinessException e) {
            e.printStackTrace();
            Assert.fail("Caught Business exception, test fail!");
            return;
        }
        System.out.println("Success ValidationService.verifyProductCreating() test");
    }

    @Test
    public void verifyClientSession() {
//        GIVEN

//        WHEN
        long idToVerify = expectedClient.getId();
        boolean verifyClientSessionResult = validationService.verifyClientSession(expectedClient, idToVerify);
//        THEN
        Assert.assertTrue(verifyClientSessionResult);
        System.out.println("Success ValidationService.verifyClientSession() test");
    }

    @Test
    public void filtratePhoneSymbols() {
//        GIVEN

//        WHEN
        Client filtratedPhoneClient = validationService.filtratePhoneSymbols(expectedClient);
//        THEN
        expectedClient.setPhone(
                expectedClient
                        .getPhone()
                        .replaceAll("[()-]", ""));
        Assert.assertEquals(expectedClient, filtratedPhoneClient);
        System.out.println("Success ValidationService.filtratePhoneSymbols() test");
    }

    @Test
    public void productNameMatchExceptionTest() {
//        GIVEN
        Map<Long, Product> productDb = new HashMap<>();
        Product expectedProduct = new Product("test product name", new BigDecimal(0.00001));
        productDb.put(null, expectedProduct);
        Mockito.when(productService.getAllProducts()).thenReturn(productDb);
//        WHEN
        try {
            validationService.verifyProductCreating(expectedProduct);
//        THEN - bad code example without assertj
        } catch (BusinessException e) {
            if (e.getMessage().equals("Product name already exist")) {
                System.out.println("Success ValidationService.verifyProductCreating() test");
                return;
            }
        }
        Assert.fail("Business exception not caught, test fail!");
    }

    @Test
    public void ageValidationExceptionTest() {
//        GIVEN
        Client clientWithForbiddenAge = new Client(
                expectedClient.getName(),
                expectedClient.getSurName(),
                1,
                expectedClient.getEmail(),
                changeExpectedClientPhone(expectedClient.getPhone())
        );
//        WHEN
        try {
            validationService.verifyClientWhileCreating(clientWithForbiddenAge);
        } catch (BusinessException e) {
            if (e.getMessage().startsWith("Client age must be from ")) {
                System.out.println("Success ageValidationException test");
                return;
            }
        }
//        THEN - bad code example without assertj
        Assert.fail("Business exception not caught, test fail!");
    }

    @Test
    public void mailNotMatchPatternExceptionTest() {
//        GIVEN
        Client clientWithForbiddenAge = new Client(
                expectedClient.getName(),
                expectedClient.getSurName(),
                expectedClient.getAge(),
                "someMailNameWithoutAT",
                changeExpectedClientPhone(expectedClient.getPhone())
        );
//        WHEN
        try {
            validationService.verifyClientWhileCreating(clientWithForbiddenAge);
        } catch (BusinessException e) {
            if (e.getMessage().equals("Inputed email not valid, try mail@example.com")) {
                System.out.println("Success mailNotMatchPatternException test");
                return;
            }
        }
//        THEN - bad code example without assertj
        Assert.fail("Business exception not caught, test fail!");
    }

    @Test
    public void phoneNotMatchPatternExceptionTest() {
//        GIVEN
        Client clientWithForbiddenAge = new Client(
                expectedClient.getName(),
                expectedClient.getSurName(),
                expectedClient.getAge(),
                expectedClient.getEmail(),
                "notPhone"
        );
//        WHEN
        try {
            validationService.verifyClientWhileCreating(clientWithForbiddenAge);
        } catch (BusinessException e) {
            if (e.getMessage().equals("inputed phone number not match to pattern")) {
                System.out.println("Success phoneNotMatchPatternException test");
                return;
            }
        }
//        THEN - bad code example without assertj
        Assert.fail("Business exception not caught, test fail!");
    }

    @Test
    public void phoneIsPresentExceptionTest() {
//        GIVEN
        Map<Long, Client> clientDb = new HashMap<>();
        clientDb.put(expectedClient.getId(), expectedClient);
        Client clientWithForbiddenAge = new Client(
                expectedClient.getName(),
                expectedClient.getSurName(),
                expectedClient.getAge(),
                expectedClient.getEmail(),
                expectedClient.getPhone()
        );
        Mockito.when(clientService.getAllClients()).thenReturn(clientDb);
//        WHEN
        try {
            validationService.verifyClientWhileCreating(clientWithForbiddenAge);
        } catch (BusinessException e) {
            if (e.getMessage().equals("Phone already exist")) {
                System.out.println("Success phoneIsPresentException test");
                return;
            }
        }
//        THEN - bad code example without assertj
        Assert.fail("Business exception not caught, test fail!");
    }

    private String changeExpectedClientPhone(String phone) {
        String changedClientPhone = phone;
        if (changedClientPhone.endsWith("0")) {
            changedClientPhone = changedClientPhone.substring(0, changedClientPhone.length() - 1).concat("9");
        } else {
            changedClientPhone = changedClientPhone.substring(0, changedClientPhone.length() - 1).concat("0");
        }
        return changedClientPhone;
    }
}