package com.romantsev.services.impl;

import com.romantsev.dao.ProductDao;
import com.romantsev.domain.Product;
import com.romantsev.services.ProductService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

    private ProductService productService;

    private Product expectedProduct;

    @Mock
    private ProductDao productDao;

    @Before
    public void setUp() {
        productService = new ProductServiceImpl(productDao);
        expectedProduct = new Product();
        expectedProduct.setId(0);
        expectedProduct.setName("Expected product");
        expectedProduct.setPrice(new BigDecimal(1.001));
    }

    @After
    public void tearDown() {
        productService = null;
        expectedProduct = null;
    }

    @Test
    public void addProductToList() {
//        GIVEN
        Mockito.when(productDao.addProduct(expectedProduct.getName(), expectedProduct.getPrice())).thenReturn(true);
//        WHEN
        boolean addProductToListResult = productService.addProductToList(expectedProduct.getName(), expectedProduct.getPrice());
//        THEN
        Assert.assertTrue(addProductToListResult);
        System.out.println("Success ProductService.addProductToList() test");
    }

    @Test
    public void modifyProduct() {
//        GIVEN
        modifyExpectedProduct();
        Mockito.when(productDao.modifyProduct(expectedProduct)).thenReturn(true);
//        WHEN
        boolean modifyProductResult = productService.modifyProduct(expectedProduct);
//        THEN
        Assert.assertTrue(modifyProductResult);
        System.out.println("Success ProductService.modifyProduct() test");
    }

    @Test
    public void removeProductFromListById() {
//        GIVEN
        Mockito.when(productDao.removeProductById(expectedProduct.getId())).thenReturn(true);
//        WHEN
        boolean removeProductFromListByIdResult = productService.removeProductFromListById(expectedProduct.getId());
//        THEN
        Assert.assertTrue(removeProductFromListByIdResult);
        System.out.println("Success ProductService.removeProductFromListById() test");
    }

    @Test
    public void getAllProducts() {
        //        GIVEN
        Map<Long, Product> expectedDb = new HashMap<>();
        expectedDb.put(expectedProduct.getId(), expectedProduct);
        expectedProduct.setId(expectedProduct.getId() + 1);
        expectedProduct.setName("Cloned client");
        expectedDb.put(expectedProduct.getId(), expectedProduct);
        Mockito.when(productDao.getAllProducts()).thenReturn(expectedDb);
//        WHEN
        Map<Long, Product> returnedDb = productService.getAllProducts();
//        THEN
        Assert.assertEquals(expectedDb, returnedDb);
        System.out.println("Success ProductService.getAllClient() test");
    }

    private void modifyExpectedProduct() {
        expectedProduct.setName("new product name");
        expectedProduct.setPrice(new BigDecimal(1001.09));
    }
}