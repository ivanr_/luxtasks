package com.romantsev.db.h2;

import org.h2.tools.Server;

import java.sql.SQLException;

public class H2ConnectorTest {
    private Server server;

    public void connect() throws SQLException {
        server = Server.createTcpServer("-tcpAllowOthers", "-tcpPassword", "adminTest").start();
    }

    public void disconnect(){
        server.stop();
    }
}
