package com.LectionsMaterial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

public class Temp {
    public static void main(String[] args) {
        String phone = "+38(097)312-12-45";
        String email = "a21312sa_@aa1.aA";
        String regexPhone = "[+]38[(]0\\d{2}[)]\\d{3}-*\\d{2}-*\\d{2}";
        String regexEmail = "[a-zA-Z]\\w{0,29}@\\w{1,15}.[a-zA-Z]\\w{1,10}";

        boolean isMatch1 = Pattern.matches(regexPhone, phone);
        boolean isMatch2 = Pattern.matches(regexEmail, email);

        System.out.println("Regex match " + isMatch1 + " phone " + phone);
        System.out.println("Regex match " + isMatch2 + " email " + email);
        BigDecimal negate = new BigDecimal(0);
        System.out.println(negate.compareTo(BigDecimal.ZERO));

        String s1 = "asd";
        String s2 = "asd1";
        String s3 = new String("asd");
        System.out.println(s1 == s2);
        System.out.println(s3.equals(s2));

        A.B b = new A.B();
        A.B.d = 11;

        List<Long> l = new ArrayList<>();
        l.add(2L);
        l.add(3L);
        l.add(4L);
        l.remove(4L);
//        l.remove(4);
        System.out.println(l.toString());
        new HashMap().values();

        String[] arrS = {"asd", "asd1"};
        arrS[0] = "asd2!";
        System.out.println(s1);
    }
}

enum C {
    a, vc
}

class A {

    static class B extends A {
        int c = 9;
        static int d = 10;
    }

    void t(int a) {
        switch (a) {
            case 1:
                System.out.println(1);
                break;
            case 2:
                System.out.println(2);
                break;
            default:
                System.out.println("exit");
        }
        float d = 2f;
        List<? extends Number> list = new ArrayList<Byte>();
    }
}

class Param<T, V> {
    T t;
    V v;

//    Param(T o){
//        t = new T();
//    }

//    void setT(T o){
//        t = o;
//    }

    void setT(V o){

    }
}