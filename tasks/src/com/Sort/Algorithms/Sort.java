package com.Sort.Algorithms;

public interface Sort{
    public int[] sort(int[] input);
}
