package com;


import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainClass {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com");
        System.out.println("Service1 init");
        Service1 service1 = context.getBean(Service1.class);
        System.out.println("Service2 init");
//        Service2 service2 = context.getBean(Service2.class);
        System.out.println("Service1 start");
        service1.text();
//        System.out.println("Service2 start");
//        service2.text();
    }
}
