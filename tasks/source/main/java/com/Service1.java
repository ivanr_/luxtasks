package com;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
class Service1 {
    @Autowired
    private Service2 service2;

    @Autowired
    Service1(Service2 service2) {
        this.service2 = service2;
        service2.text();
    }

    @PostConstruct
    void text() {
        System.out.println("text service1");
        service2.text();
    }
}
